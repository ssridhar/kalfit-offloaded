#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <map>
#include <array>
#include <unistd.h>
#include "tbb/tbb.h"

#include "oldfit/Types.h"
#include "oldfit/Predict.h"
#include "oldfit/Update.h"
#include "oldfit/Bismooth.h"

#include "utilities/Tools.h"
#include "utilities/Timer.h"
#include "utilities/FileReader.h"

// New types
#include "nfit/nTypes.h"
#include "nfit/nPredict.h"
#include "nfit/nUpdate.h"
#include "nfit/nSmoother.h"

#define TBB_ON
// #define CALLGRIND_ON
#ifdef CALLGRIND_ON
#include <valgrind/callgrind.h>
#endif

int main(int argc, char **argv) {
  std::string foldername = "events";
  std::vector<std::vector<uint8_t>> files;
  std::vector<std::vector<Instance>> events;
  int bitEpsilonAllowed = 50;
  bool printFitNodesWhenDifferent = false;
  int numExperiments = 100;
  unsigned int maxNumFilesToOpen = 0;
  int minNumTracksInInstance = 0;
  bool checkResults = false;
  bool allEqual = true;
  unsigned int verbose = 0;
  bool experimentMode = false;
  bool vectorised = true;
  TimePrinter p (true);
  FileReader fr;
  char c;

  while (true) {
    c = getopt (argc, argv, "b:f:n:h?m:c:v:p:es:");
    if (c == -1 or c == 255 /* power8 */) break;

    switch (c) {
      case 'n':
        numExperiments = atoi(optarg);
        break;
      case 'f':
        foldername = optarg;
        break;
      case 'b':
        bitEpsilonAllowed = atoi(optarg);
        break;
      case 'm':
        maxNumFilesToOpen = atoi(optarg);
        break;
      case 'c':
        checkResults = (bool) atoi(optarg);
        break;
      case 'v':
        verbose = atoi(optarg);
        break;
      case 's':
        vectorised = not ((bool) atoi(optarg));
        break;
      case 'p':
      {
        const unsigned int percentile = atoi(optarg);
        p = TimePrinter(true, percentile, 100 - percentile);
        break;
      }
      case 'e':
        experimentMode = true;
        break;
      case 'h':
      case '?':
        std::cout << "kalfit [-f foldername=events] [-n numExperiments=100]"
          << " [-s sequential=0 (vectorised)]"
          << " [-m maxNumFilesToOpen=0 (all)] [-e (print git checkout version=false)]"
          << " [-p topBottomPercentile=0] [-m maxNumFilesToOpen=0 (all)]"
          << " [-c checkResults=0 (0,1) [-b bitEpsilonAllowed=50] [-v verbose=0 (0,1,2)]]" << std::endl;
        return 0;
      default:
        break;
    }
  }

  if (experimentMode) {
    std::cout << "Reported version (git): " << shellexec("git rev-parse HEAD");
  }

  std::cout << "Running options: ";
  for (int i=0; i<argc; ++i) std::cout << argv[i] << " ";
  std::cout << std::endl << std::endl;

  // Read events
  fr.readFilesFromFolder(files, foldername, maxNumFilesToOpen);
  for (const auto& f : files) events.push_back(translateFileIntoEvent(f));

  assert(events.size() > 0);

  std::cout << "Read " << events.size() << " events" << std::endl;

  // Print some info for the events
  for (int i=0; i<events.size(); ++i) {
    const auto& event = events[i];

    std::vector<GroupInfo> groups;
    std::cout << "#" << i << ":" << std::endl;
    printInfo(event, groups);
  }

  // We should run "as the framework should"
  // So we run many events, scheduled with tbb exploiting parallelism
  // in the best way possible within each event
  std::vector<tbb::concurrent_vector<std::pair<int, Timer>>> timers (2);
  std::cout << "Running experiments ..." << std::endl;

#ifdef TBB_ON
  tbb::parallel_for (static_cast<unsigned int>(0), static_cast<unsigned int>(numExperiments),
  [&] (unsigned int i) {
#else
  for (int i=0; i<numExperiments; ++i) {
#endif

    // Note: Copying the event requires too much memory, rather copy only what we need
    //       We copy so as not to pollute the events
    const unsigned int eventno = i % events.size(); // Round robin event selection
    auto& event = events[eventno];

    int instno = 0;
    for (int j=0; j<event.size(); ++j) {
      auto& instance = event[j];

      // Prepare experiment
      // Note: Copy the ntracks, which is exactly what we need for our experiment
      std::vector<nTrack> ntracks = instance.ntracks;
      const bool doForwardFit = instance.doForwardFit;
      const bool doBackwardFit = instance.doBackwardFit;
      const bool doBismooth = instance.doBismooth;
      const bool isOutlier = instance.isOutlier;

      // Prepare the memory manager
      const int totalNumberOfNodes = [&] {
        int acc = 0;
        for (auto& t : instance.ntracks) {
          acc += t.m_nodes.size();
        }
        return acc;
      } ();
      MemManager memManagerSequential(41, VECTOR_WIDTH, totalNumberOfNodes * 2);
      MemManager memManagerVectorised(41, VECTOR_WIDTH, totalNumberOfNodes * 2);
      MemManager memManagerSequentialBackward(41, VECTOR_WIDTH, totalNumberOfNodes * 2);
      MemManager memManagerVectorisedBackward(41, VECTOR_WIDTH, totalNumberOfNodes * 2);

      if (!isOutlier) {

        // Initialization
        // Dumb dynamic scheduler, upon tracks ordered by size, from more nodes to less nodes
        std::vector<std::reference_wrapper<nTrack>> ordered_ntracks (ntracks.begin(), ntracks.end());
        std::sort(ordered_ntracks.begin(), ordered_ntracks.end(), [] (const nTrack& n0, const nTrack& n1) {
          return n0.m_nodes.size() > n1.m_nodes.size();
        });

        // Initialise scheduler machinery
        std::array<SchItem, VECTOR_WIDTH> processingTracks;
        std::vector<unsigned int> slots;
        for (int i=0; i<VECTOR_WIDTH; ++i) { slots.push_back(i); }

        // Forward fit

        auto tFit = Timer();

        // Forward fit (not outliers)
        // Do iterations until all tracks have info upstream

        if (doForwardFit) {
          std::for_each(ordered_ntracks.begin(), ordered_ntracks.end(), [&] (nTrack& ntrack) {
            std::vector<nFitNode>& track = ntrack.m_nodes;
            nFitNode& node = track.front();
            node.getFit<Fit::Forward>().m_states.setBasePointer(memManagerSequential.getNextElement());

            npredictInitialise<Fit::Forward>(node, ntrack.m_initialForwardCovariance);
            nupdate<Fit::Forward>(node);

            // Before m_forwardUpstream
            for (int i=1; i<=ntrack.m_forwardUpstream; ++i) {
              nFitNode& node = track[i];
              nFitNode& prevnode = track[i-1];
              node.getFit<Fit::Forward>().m_states.setBasePointer(memManagerSequential.getNextElement());
              
              npredict<Fit::Forward, false>(node, prevnode);
              nupdate<Fit::Forward>(node);
            }
          });
        }


        tFit.start();

#ifdef CALLGRIND_ON
        CALLGRIND_TOGGLE_COLLECT;
#endif
        
        if (doForwardFit) {
          if (!vectorised) {
            // Non-vectorised version
            std::for_each(ordered_ntracks.begin(), ordered_ntracks.end(), [&] (nTrack& ntrack) {
              std::vector<nFitNode>& track = ntrack.m_nodes;
              // After m_forwardUpstream
              for (int i=ntrack.m_forwardUpstream+1; i<track.size(); ++i) {
                nFitNode& node = track[i];
                nFitNode& prevnode = track[i-1];
                node.getFit<Fit::Forward>().m_states.setBasePointer(memManagerSequential.getNextElement());
                
                npredict<Fit::Forward, true>(node, prevnode);
                nupdate<Fit::Forward>(node);
              }
            });
          }
          else {
            // Vectorised version
            // Attempt to take the tracks in batches of vector_size
            memManagerVectorised.getNewVector();

            auto trackIterator = ordered_ntracks.begin();
            while (trackIterator != ordered_ntracks.end()) {
              // Grab a track more
              nTrack& ntrack = *trackIterator;
              std::vector<nFitNode>& track = ntrack.m_nodes;
              const int slot = slots.back();
              slots.pop_back();
              
              processingTracks[slot] = SchItem(&track,
                ntrack.m_forwardUpstream + 1,
                ntrack.m_forwardUpstream,
                slot);

              // Copy starting status in an empty slot
              nStates newslot (memManagerVectorised.getLastVector() + slot, VECTOR_WIDTH);
              newslot.m_updatedCovariance.copy(track[ntrack.m_forwardUpstream].getFit<Fit::Forward>().m_states.m_updatedCovariance);
              // track[ntrack.m_forwardUpstream].getFit<Fit::Forward>().m_states.setBasePointer(newslot);

              while (slots.size() == 0) {
                // Start processing the nodes in batches
                // Prepare the storage location
                const nStates lastStates (memManagerVectorised.getLastVector(), VECTOR_WIDTH);
                const nStates currentStates (memManagerVectorised.getNewVector(), VECTOR_WIDTH);
                for (int i=0; i<VECTOR_WIDTH; ++i) {
                  processingTracks[i].node->getFit<Fit::Forward>().m_states.setBasePointer(currentStates.basePointer + i);
                }

                // predict
                npredict_vec<Fit::Forward>::op<VECTOR_WIDTH> (
                  processingTracks,
                  lastStates.m_updatedCovariance.basePointer,
                  currentStates.m_predictedCovariance.basePointer
                );

                // update
                nupdate_vec<VECTOR_WIDTH> (
                  processingTracks,
                  currentStates.m_predictedState.basePointer,
                  currentStates.m_predictedCovariance.basePointer,
                  currentStates.m_updatedState.basePointer,
                  currentStates.m_updatedCovariance.basePointer,
                  currentStates.m_chi2
                );

                // Update all tracks information
                // and check for finished tracks
                std::for_each(processingTracks.begin(), processingTracks.end(), [&] (SchItem& s) {
                  // Advance to next node
                  ++s.node;
                  ++s.prevnode;

                  if (s.node == s.track->end()) {
                    // Move data out and update pointers
                    nStates store (memManagerSequential.getNextElement(), VECTOR_WIDTH);
                    store.m_updatedCovariance.copy(s.track->back().getFit<Fit::Forward>().m_states.m_updatedCovariance);
                    s.track->back().getFit<Fit::Forward>().m_states.m_updatedCovariance.setBasePointer(store.m_updatedCovariance);

                    // Free slot and remove finished track
                    slots.push_back(s.slot);
                  }
                });
              }

              trackIterator++;
            }

            // Process the resting nodes and tracks without vectors
            for (int k=0; k<VECTOR_WIDTH; ++k) {
              if (std::find(slots.begin(), slots.end(), k) == slots.end()) {
                auto& t = processingTracks[k];
                auto& node = t.node;
                auto& prevnode = t.prevnode;
                auto* track = t.track;

                while (node != track->end()) {
                  node->getFit<Fit::Forward>().m_states.setBasePointer(memManagerSequential.getNextElement());
                    
                  npredict<Fit::Forward, true>(*node, *prevnode);
                  nupdate<Fit::Forward>(*node);
                  
                  ++prevnode;
                  ++node;
                }
              }
            }
          }
        }

#ifdef CALLGRIND_ON
        CALLGRIND_TOGGLE_COLLECT;
#endif

        tFit.stop();

        if (doBackwardFit) {
          std::for_each(ordered_ntracks.begin(), ordered_ntracks.end(), [&] (nTrack& ntrack) {
            std::vector<nFitNode>& track = ntrack.m_nodes;
            nFitNode& node = track.back();
            node.getFit<Fit::Backward>().m_states.setBasePointer(memManagerSequentialBackward.getNextElement());

            npredictInitialise<Fit::Backward>(node, ntrack.m_initialBackwardCovariance);
            nupdate<Fit::Backward>(node);

            // Before m_backwardUpstream
            for (int i=1; i<=ntrack.m_backwardUpstream; ++i) {
              const int element = ntrack.m_nodes.size() - i - 1;
              nFitNode& node = track[element];
              nFitNode& prevnode = track[element + 1];
              node.getFit<Fit::Backward>().m_states.setBasePointer(memManagerSequentialBackward.getNextElement());
              
              npredict<Fit::Backward, false>(node, prevnode);
              nupdate<Fit::Backward>(node);
            }
          });
        }

        tFit.start();

#ifdef CALLGRIND_ON
        CALLGRIND_TOGGLE_COLLECT;
#endif

        if (doBackwardFit) {
          if (!vectorised) {
            // Non vectorised version
            std::for_each(ordered_ntracks.begin(), ordered_ntracks.end(), [&] (nTrack& ntrack) {
              std::vector<nFitNode>& track = ntrack.m_nodes;
              // After m_backwardUpstream
              for (int i=ntrack.m_backwardUpstream+1; i<track.size(); ++i) {
                const int element = ntrack.m_nodes.size() - i - 1;
                nFitNode& node = track[element];
                nFitNode& prevnode = track[element + 1];
                node.getFit<Fit::Backward>().m_states.setBasePointer(memManagerSequentialBackward.getNextElement());
                
                npredict<Fit::Backward, true>(node, prevnode);
                nupdate<Fit::Backward>(node);
              }
            });
          } else {
            // Vectorised version
            // Reinitialize slots
            slots.clear();
            for (int i=0; i<VECTOR_WIDTH; ++i) { slots.push_back(i); }

            // Backward fit
            if (doBackwardFit) {
              memManagerVectorisedBackward.getNewVector();
              auto trackIterator = ordered_ntracks.begin();
              while (trackIterator != ordered_ntracks.end()) {
                // Grab a total of VECTOR_WIDTH tracks
                nTrack& ntrack = *trackIterator;
                std::vector<nFitNode>& track = ntrack.m_nodes;
                const int slot = slots.back();
                slots.pop_back();
                
                processingTracks[slot] = SchItem(&track,
                  track.size() - ntrack.m_backwardUpstream - 2,
                  track.size() - ntrack.m_backwardUpstream - 1,
                  slot);

                // Copy starting status in an empty slot
                const int element = track.size() - ntrack.m_backwardUpstream - 1;
                nStates newslot (memManagerVectorisedBackward.getLastVector() + slot, VECTOR_WIDTH);
                newslot.m_updatedCovariance.copy(track[element].getFit<Fit::Backward>().m_states.m_updatedCovariance);
                // track[element].getFit<Fit::Backward>().m_states.setBasePointer(newslot);

                while (slots.size() == 0) {
                  // Start processing the nodes in batches
                  // Prepare the storage location
                  const nStates lastStates (memManagerVectorisedBackward.getLastVector(), VECTOR_WIDTH);
                  const nStates currentStates (memManagerVectorisedBackward.getNewVector(), VECTOR_WIDTH);
                  for (int i=0; i<VECTOR_WIDTH; ++i) {
                    processingTracks[i].node->getFit<Fit::Backward>().m_states.setBasePointer(currentStates.basePointer + i);
                  }

                  // predict
                  npredict_vec<Fit::Backward>::op<VECTOR_WIDTH> (
                    processingTracks,
                    lastStates.m_updatedCovariance.basePointer,
                    currentStates.m_predictedCovariance.basePointer
                  );

                  // update
                  nupdate_vec<VECTOR_WIDTH> (
                    processingTracks,
                    currentStates.m_predictedState.basePointer,
                    currentStates.m_predictedCovariance.basePointer,
                    currentStates.m_updatedState.basePointer,
                    currentStates.m_updatedCovariance.basePointer,
                    currentStates.m_chi2
                  );

                  // Update all tracks information
                  // and check for finished tracks
                  std::for_each(processingTracks.begin(), processingTracks.end(), [&] (SchItem& s) {
                    // Advance to next node
                    --s.node;
                    --s.prevnode;

                    if (s.prevnode == s.track->begin()) {
                      // Move data out and update pointers
                      nStates store (memManagerSequentialBackward.getNextElement(), VECTOR_WIDTH);
                      store.m_updatedCovariance.copy(s.track->front().getFit<Fit::Backward>().m_states.m_updatedCovariance);
                      s.track->front().getFit<Fit::Backward>().m_states.m_updatedCovariance.setBasePointer(store.m_updatedCovariance);

                      // Free slot and remove finished track
                      slots.push_back(s.slot);
                    }
                  });
                }

                trackIterator++;
              }

              // Process the resting nodes and tracks without vectors
              for (int k=0; k<VECTOR_WIDTH; ++k) {
                if (std::find(slots.begin(), slots.end(), k) == slots.end()) {
                  auto& t = processingTracks[k];
                  auto& node = t.node;
                  auto& prevnode = t.prevnode;
                  auto* track = t.track;

                  while (prevnode != track->begin()) {
                    node->getFit<Fit::Backward>().m_states.setBasePointer(memManagerSequentialBackward.getNextElement());
                      
                    npredict<Fit::Backward, true>(*node, *prevnode);
                    nupdate<Fit::Backward>(*node);
                    
                    --prevnode;
                    --node;
                  }
                }
              }
            }
          }
        }

#ifdef CALLGRIND_ON
        CALLGRIND_TOGGLE_COLLECT;
#endif

        tFit.stop();

        // Calculate the chi2 a posteriori
        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
          std::vector<nFitNode>& track = ntrack.m_nodes;

          if (doForwardFit or doBackwardFit) {
            ntrack.m_ndof = -track[0].m_parent_nTrackParameters;
            for (int k=0; k<track.size(); ++k) {
              const nFitNode& node = track[k];

              if (node.m_type == HitOnTrack) {
                ++ntrack.m_ndof;
                if (doForwardFit)  ntrack.m_forwardFitChi2  += node.getChi2<Fit::Forward>();
                if (doBackwardFit) ntrack.m_backwardFitChi2 += node.getChi2<Fit::Backward>();
              }
            }
          }
        });

        // // Smoother
        // if (doBismooth) {
          auto tSmoother = Timer();
          tSmoother.start();
        //   std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
        //     for (int k=0; k<ntrack.m_nodes.size(); ++k) {
        //       nsmoother(ntrack.m_nodes[k], ntrack.m_forwardUpstream < k, ntrack.m_backwardUpstream > k);
        //     }
        //   });
          tSmoother.stop();

          // int numberOfSmoothers = 0;
        //   std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) { numberOfSmoothers += ntrack.m_nodes.size(); });
          // timers[1].push_back(std::make_pair(numberOfSmoothers, tSmoother));
          timers[1].push_back(std::make_pair(10, tSmoother));
        // }

        // Add fit timer to list
        int numberOfFits = 0;
        std::for_each(ntracks.begin(), ntracks.end(), [&] (nTrack& ntrack) {
          if (doForwardFit) {
            numberOfFits += ntrack.m_nodes.size();
            if (isOutlier) {
              numberOfFits -= ntrack.outlier;
            }
          }

          if (doBackwardFit) {
            if (isOutlier) {
              numberOfFits += ntrack.outlier;
            } else {
              numberOfFits += ntrack.m_nodes.size();
            }
          }
        });
        timers[0].push_back(std::make_pair(numberOfFits, tFit));

        // Convert back
        std::vector<std::vector<FitNode>> tracks;
        for (const auto& ntrack : ntracks) {
          std::vector<FitNode> track;
          for (const auto& node : ntrack.m_nodes) {
            track.push_back(node);
          }

          if (doForwardFit) {
            track.back().m_totalChi2[Forward] = ChiSquare(ntrack.m_forwardFitChi2, ntrack.m_ndof);
          }
          if (doBackwardFit) {
            track.front().m_totalChi2[Backward] = ChiSquare(ntrack.m_backwardFitChi2, ntrack.m_ndof);
          }
          tracks.push_back(track);
        }

        // Check results are correct
        if (checkResults) {
          for (int k=0; k<tracks.size(); ++k) {
            std::vector<FitNode>& nodes  = tracks[k];
            std::vector<FitNode>& result = instance.expectedResult[k];

            if (doForwardFit && doBackwardFit && !isOutlier) {
              for (int l=0; l<nodes.size(); ++l) {
                int compareChi2 = -1;
                if (l==0 && doBackwardFit) compareChi2 = Backward;
                else if (l==nodes.size()-1 && doForwardFit) compareChi2 = Forward;

                const bool comparison = compare(nodes[l], result[l], pow(2, bitEpsilonAllowed), (verbose > 0), compareChi2);
                if (!comparison) {
                  std::cout << "Mismatch in event #" << eventno << ", instance #" << j
                    << " (" << (isOutlier ? "outlier" : "convergence") << ")"
                    << " (" << doForwardFit << ", " << doBackwardFit << ", " << doBismooth << ")"
                    << ", track " << k << ", node " << l << std::endl;
                  allEqual = false;

                  if (verbose > 1) {
                    print(nodes[l]);
                    std::cout << std::endl;
                    print(result[l]);
                  }
                }
              }
            }
          }
        }
      }
    }
#ifdef TBB_ON
  });
#else
  }
#endif
  
#ifdef CALLGRIND_ON
  CALLGRIND_DUMP_STATS;
#endif

  // Get estimate on number of threads being used by tbb
  const int numberOfThreads = tbb::task_scheduler_init::default_num_threads();

  // Indicative number of predicted, filtered and smoothed executed
  // Deprecated
  unsigned long long int deprecatedTotalPredicted = 0, deprecatedTotalFiltered = 0, deprecatedTotalSmoothed = 0;
  for (int i=0; i<numExperiments; ++i) {
    const unsigned int eventno = i % events.size();
    const auto& event = events[eventno];
    for (const auto& instance : event) {
      for (const auto& track : instance.tracks) {
        if (instance.doForwardFit) {
          deprecatedTotalPredicted += track.size();
          deprecatedTotalFiltered  += track.size();
        }

        if (instance.doBackwardFit) {
          deprecatedTotalPredicted += track.size();
          deprecatedTotalFiltered += track.size();
        }

        if (instance.doBismooth) {
          deprecatedTotalSmoothed += track.size();
        }
      }
    }
  }

  std::cout << std::endl << std::endl;

  std::map<std::string, double> fitsTimes     = p.printWeightedTimer(timers[0], "Forward and Backward fit");
  std::map<std::string, double> smootherTimes = p.printWeightedTimer(timers[1], "Smoother");

  const unsigned long long totalFitted   = [&] {unsigned long long t=0; for (const auto& wt : timers[0]) t += wt.first; return t;} ();
  const unsigned long long totalSmoothed = [&] {unsigned long long t=0; for (const auto& wt : timers[1]) t += wt.first; return t;} ();

  std::cout << "Total statistics: " << totalFitted << " fitted, " << totalSmoothed << " smoothed" << std::endl << std::endl;
  std::cout << "(Deprecated statistics: " << deprecatedTotalFiltered << " filtered, " << deprecatedTotalPredicted << " predicted, "
    << deprecatedTotalSmoothed << " smoothed)" << std::endl << std::endl;

  if (checkResults) {
    if (allEqual) {
      std::cout << "Check results succeeded!" << std::endl
        << "All of the experiments gave the expected results" << std::endl << std::endl;
    }
    else {
      std::cout << "Check results failed" << std::endl
        << "Some experiments did not yield the expected results" << std::endl
        << "Try running with -c 1 -v 1 to see what happens" << std::endl << std::endl;
    }
  }

  std::cout << "Fit timers mean: " << fitsTimes["mean"] << " sum: " << fitsTimes["sum"]
    << " min: " << fitsTimes["min"] << " max: " << fitsTimes["max"] << " stddev: " << fitsTimes["stdev"] << std::endl
    << "Smoother timers mean: " << smootherTimes["mean"] << " sum: " << smootherTimes["sum"]
    << " min: " << smootherTimes["min"] << " max: " << smootherTimes["max"] << " stddev: " << smootherTimes["stdev"] << std::endl;

  std::cout << std::endl
    << "tbb default_num_threads reports execution with " << numberOfThreads << " threads" << std::endl
    << "Throughput per processor, estimated total throughput:" << std::endl
    << " Forward and Backward fit: " << 1.0 / fitsTimes["mean"] << " / s, "
    << numberOfThreads * (1.0 / fitsTimes["mean"]) << " / s" << std::endl
    << " Smoother: " << 1.0 / smootherTimes["mean"]  <<  " / s, "
    << numberOfThreads * (1.0 / smootherTimes["mean"]) << " / s" << std::endl;

  return 0;
}
