#pragma once

#include "CholeskyDecomp.h"

class nTrackVector;
class nTrackSymMatrix;

struct Matrix5x5 {
  double fArray[25];
  double& operator()(const int x, const int y) {return fArray[x*5+y]; }
  double operator()(const int x, const int y) const {return fArray[x*5+y]; }
  
  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}
};

struct Matrix1x5 {
  double fArray[5];
  double& operator()(const int x) {return fArray[x]; }
  double operator()(const int x) const {return fArray[x]; }

  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}

  void operator=(const nTrackVector& mB);

  Matrix1x5 () = default;
  Matrix1x5 (double a0, double a1, double a2, double a3, double a4) {
    fArray[0] = a0;
    fArray[1] = a1;
    fArray[2] = a2;
    fArray[3] = a3;
    fArray[4] = a4;
  }
};

struct SymMatrix5x5 {
  double fArray[15];

  bool InvertChol() {
    ROOT::Math::CholeskyDecomp<double, 5> decomp(fArray);
    return decomp.Invert(fArray);
  }

  double& operator() (const int row, const int col) {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  double operator()(const int row, const int col) const {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}

  void operator=(const nTrackSymMatrix& mB);
};

Matrix1x5 operator*(const Matrix5x5& mA, const Matrix1x5& mB);
Matrix1x5 operator+(const Matrix1x5& mA, const Matrix1x5& mB);
Matrix1x5 operator-(const Matrix1x5& mA, const Matrix1x5& mB);
SymMatrix5x5 operator+(const SymMatrix5x5& mA, const SymMatrix5x5& mB);
void operator+=(SymMatrix5x5& mA, const SymMatrix5x5& mB);
double operator*(const Matrix1x5& mA, const Matrix1x5& mB);

typedef Matrix1x5 TrackVector;
typedef Matrix1x5 TrackProjectionMatrix;
typedef Matrix5x5 TrackMatrix;
typedef SymMatrix5x5 TrackSymMatrix;
