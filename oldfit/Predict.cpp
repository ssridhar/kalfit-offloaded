#include "Predict.h"

void transportCovariance (
  const TrackMatrix& F,
  const TrackSymMatrix& origin,
  TrackSymMatrix& target
) {
  bool isLine = F(0,4)==0;
  if (!isLine) {
    math_similarity_5_5(origin.fArray, F.fArray, target.fArray);
  } else {
    target = origin;
    if (F(0,2) != 0 or F(1,3) != 0) {
      target(0,0) += 2 * origin(2,0) * F(0,2) + origin(2,2) * F(0,2) * F(0,2);
      target(2,0) += origin(2,2) * F(0,2);
      target(1,1) += 2 * origin(3,1) * F(1,3) + origin(3,3) * F(1,3) * F(1,3);
      target(3,1) += origin(3,3) * F(1,3);
      target(1,0) += origin(2,1) * F(0,2) + origin(3,0) * F(1,3) + origin(3,2) * F(0,2) * F(1,3);
      target(2,1) += origin(3,2) * F(1,3);
      target(3,0) += origin(3,2) * F(0,2);
    }
  }
}

void predict (
  FitNode& node,
  const FitNode* const prevNode,
  const bool hasPrevNode,
  const int direction
) {
  node.m_predictedState[direction].m_z = node.m_refVector.m_z;
  TrackSymMatrix& stateCov = node.m_predictedState[direction].m_covariance;
  bool statevecUpdated = false;

  if (hasPrevNode) {
    const State& previousState = prevNode->m_filteredState[direction];
    
    if (!node.m_hasInfoUpstream[direction]) {
      // just _copy_ the covariance matrix from upstream, assuming
      // that this is the seed matrix. (that saves us from copying
      // the seed matrix to every state from the start.
      stateCov = previousState.m_covariance;
      if (direction == Backward) {
        node.m_predictedState[direction].m_stateVector = node.m_filteredState[Forward].m_stateVector;
        statevecUpdated = true;
      }
    } else {
      statevecUpdated = true;

      if (direction==Forward) {
        // Matrix5x5 * Matrix1x5 + Matrix1x5
        node.m_predictedState[direction].m_stateVector = node.m_transportMatrix * previousState.m_stateVector + node.m_transportVector;
        transportCovariance(node.m_transportMatrix, previousState.m_covariance, stateCov);
        stateCov += node.m_noiseMatrix;
      } else {
        const TrackMatrix& invF = prevNode->m_invertTransportMatrix;
        TrackSymMatrix tempCov = previousState.m_covariance + prevNode->m_noiseMatrix;
        node.m_predictedState[direction].m_stateVector = invF * (previousState.m_stateVector - prevNode->m_transportVector);
        transportCovariance(invF, tempCov, stateCov);
      }
    }
  }

  if (not statevecUpdated) {
    node.m_predictedState[direction].m_stateVector = node.m_refVector.m_parameters;
  }
}
