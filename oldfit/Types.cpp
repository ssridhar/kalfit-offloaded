#include "Types.h"
#include "../nfit/nTypes.h"

void operator+=(ChiSquare& c0, const ChiSquare& c1) {
  c0.m_chi2 += c1.m_chi2;
  c0.m_nDoF += c1.m_nDoF;
}

void State::operator=(const nState& s1) {
  m_stateVector = s1.m_stateVector;
  m_covariance = s1.m_covariance;
}

void StateVector::operator=(const nStateVector& s1) {
  m_parameters = s1.m_parameters;
  m_z = s1.m_z;
}

Node::Node (const nFitNode& node) 
  : m_type(node.m_type), 
  m_refIsSet(node.m_refIsSet), 
  m_measurement(node.m_measurement), 
  m_residual(node.m_residual), 
  m_errResidual(node.m_errResidual), 
  m_errMeasure(node.m_errMeasure), 
  m_projectionMatrix(node.m_projectionMatrix) {

  m_state = node.m_state;
  m_refVector = node.m_refVector;
}

FitNode::FitNode (const nFitNode& node)
  : Node(node),
  m_transportVector(node.m_transportVector),
  m_noiseMatrix(node.m_noiseMatrix),
  m_transportMatrix(node.m_forwardFit.m_transportMatrix),
  m_invertTransportMatrix(node.m_backwardFit.m_transportMatrix),
  m_refResidual(node.m_refResidual),
  m_parent_nTrackParameters(node.m_parent_nTrackParameters)
  {
    m_predictedState[Forward].m_stateVector = node.getState<Fit::Forward, Fit::Predicted, Fit::StateVector>();
    m_predictedState[Forward].m_covariance = node.getState<Fit::Forward, Fit::Predicted, Fit::Covariance>();
    m_filteredState[Forward].m_stateVector = node.getState<Fit::Forward, Fit::Updated, Fit::StateVector>();
    m_filteredState[Forward].m_covariance = node.getState<Fit::Forward, Fit::Updated, Fit::Covariance>();

    m_predictedState[Backward].m_stateVector = node.getState<Fit::Backward, Fit::Predicted, Fit::StateVector>();
    m_predictedState[Backward].m_covariance = node.getState<Fit::Backward, Fit::Predicted, Fit::Covariance>();
    m_filteredState[Backward].m_stateVector = node.getState<Fit::Backward, Fit::Updated, Fit::StateVector>();
    m_filteredState[Backward].m_covariance = node.getState<Fit::Backward, Fit::Updated, Fit::Covariance>();

    m_hasInfoUpstream[0] = node.m_hasInfoUpstream[0];
    m_hasInfoUpstream[1] = node.m_hasInfoUpstream[1];
  }
