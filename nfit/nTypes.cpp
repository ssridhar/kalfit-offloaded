#include "nTypes.h"

nState::nState (const State& state) {
  m_stateVector = state.m_stateVector;
  m_covariance = state.m_covariance;
}

nStateVector::nStateVector (const StateVector& state) {
  m_parameters = state.m_parameters;
  m_z = state.m_z;
}

nFitParameters::nFitParameters (const FitNode& node, const int& direction) {
  // m_predictedState = node.m_predictedState[direction];
  // m_updatedState = node.m_filteredState[direction];
  m_transportMatrix = (direction==Forward ? node.m_transportMatrix : node.m_invertTransportMatrix);
  // TODO
  // m_chi2 = node.m_deltaChi2[direction].m_chi2;
  m_hasInfoUpstream = (node.m_hasInfoUpstream[direction] == True ? true : false);
}

nNode::nNode (const FitNode& node) {
  m_type = node.m_type;
  m_state = node.m_state;
  m_refVector = node.m_refVector;
  m_refIsSet = node.m_refIsSet;
  m_measurement = node.m_measurement;
  m_residual = node.m_residual;
  m_errResidual = node.m_errResidual;
  m_errMeasure = node.m_errMeasure;
  m_projectionMatrix = node.m_projectionMatrix;
}

nFitNode::nFitNode (const FitNode& node)
  : nNode(node), m_forwardFit(node, Forward), m_backwardFit(node, Backward),
  m_noiseMatrix(node.m_noiseMatrix), m_transportVector(node.m_transportVector),
  m_refResidual(node.m_refResidual), m_parent_nTrackParameters(node.m_parent_nTrackParameters) {
  // Just to be able to translate it back
  m_hasInfoUpstream[0] = node.m_hasInfoUpstream[0];
  m_hasInfoUpstream[1] = node.m_hasInfoUpstream[1];
}

nTrack::nTrack (
  const std::vector<FitNode>& track,
  int trackNumber
) : m_trackNumber(trackNumber) {
  // Generate the nodes
  for (const auto& node : track) {
    m_nodes.push_back(nFitNode(node));
  }
  m_nodes.back().m_isLast = true;

  // Copy only the initial forward predicted and backward predicted covariance
  m_initialForwardCovariance  = track.front().m_predictedState[0].m_covariance;
  m_initialBackwardCovariance = track.back().m_predictedState[1].m_covariance;

  // m_nodes.front().getFit<Fit::Forward>().m_states = nStates(memManager.getNextElement(), VECTOR_WIDTH);
  // m_nodes.back().getFit<Fit::Backward>().m_states = nStates(memManager.getNextElement(), VECTOR_WIDTH);
  // for (int i=0; i<15; ++i) {
  //   m_nodes.front().getState<Fit::Forward, Fit::Predicted, Fit::Covariance>()[i] = track.front().m_predictedState[0].m_covariance[i];
  //   m_nodes.back().getState<Fit::Backward, Fit::Predicted, Fit::Covariance>()[i] = track.back().m_predictedState[1].m_covariance[i];
  // }

  m_forwardUpstream = 0;
  m_backwardUpstream = track.size() - 1;

  bool foundForward = false;
  bool foundBackward = false;
  for (int i=0; i<track.size(); ++i) {
    const int reverse_i = track.size() - i - 1;

    if (!foundForward && track[i].m_type == HitOnTrack) {
      foundForward = true;
      m_forwardUpstream = i;
    }

    if (!foundBackward && track[reverse_i].m_type == HitOnTrack) {
      foundBackward = true;
      m_backwardUpstream = i;
    }
  }
}
