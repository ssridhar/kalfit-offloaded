#pragma once

#include <vector>
#include <functional>
#include "../oldfit/MatrixTypes.h"
#include "../oldfit/Types.h"
#include "nStore.h"

#define _2D(i) (i * VECTOR_WIDTH + j)
#define _aligned __attribute__((aligned(64)))

#ifdef __INTEL_COMPILER
typedef double * const __restrict__ __attribute__((align_value(64))) double_ptr_64;
typedef const double * const __restrict__ __attribute__((align_value(64))) double_ptr_64_const;
#else
typedef double * const __restrict__ double_ptr_64;
typedef const double * const __restrict__ double_ptr_64_const;
#endif

// Names for templates
namespace Fit {
  class Backward;
  class Forward;
  class Predicted;
  class Updated;
  class StateVector;
  class Covariance;
}

/**
 * @brief      Our flamant AOSOA
 */
struct nStates {
  int stride = VECTOR_WIDTH;
  double* basePointer;
  nTrackVector m_predictedState;
  nTrackVector m_updatedState;
  nTrackSymMatrix m_predictedCovariance;
  nTrackSymMatrix m_updatedCovariance;
  double* m_chi2;

  nStates () = default;
  nStates (const nStates& copy) = default;
  nStates (double* basePointer, const int stride) :
    stride(stride) {
    setBasePointer(basePointer);
  }

  void setBasePointer (double* p) {
    basePointer = p;
    m_predictedState = nTrackVector(p, stride); p += 5 * stride;
    m_updatedState   = nTrackVector(p, stride); p += 5 * stride;
    m_predictedCovariance = nTrackSymMatrix(p, stride); p += 15 * stride;
    m_updatedCovariance   = nTrackSymMatrix(p, stride); p += 15 * stride;
    m_chi2 = p;
  }

  void setBasePointer (const nStates& s) {
    double* p = s.basePointer;
    basePointer = p;
    m_predictedState = nTrackVector(p, stride); p += 5 * stride;
    m_updatedState   = nTrackVector(p, stride); p += 5 * stride;
    m_predictedCovariance = nTrackSymMatrix(p, stride); p += 15 * stride;
    m_updatedCovariance   = nTrackSymMatrix(p, stride); p += 15 * stride;
    m_chi2 = p;
  }

  void copy (const nStates& s) {
    m_predictedState.copy(s.m_predictedState);
    m_updatedState.copy(s.m_updatedState);
    m_predictedCovariance.copy(s.m_predictedCovariance);
    m_updatedCovariance.copy(s.m_updatedCovariance);
    *m_chi2 = *(s.m_chi2);
  }
};

// Deprecated
struct nState {
  TrackVector m_stateVector _aligned;
  TrackSymMatrix m_covariance _aligned;

  nState () = default;
  nState (const State& state);
};

struct nStateVector {
  TrackVector m_parameters _aligned;
  double      m_z _aligned;

  nStateVector () = default;
  nStateVector (const StateVector& state);
};

struct nFitParameters {
  nStates m_states;
  TrackMatrix m_transportMatrix _aligned;
  bool m_hasInfoUpstream;

  nFitParameters (const nFitParameters& node) = default;
  nFitParameters (const FitNode& node, const int& direction);
};

struct nNode {
  Type m_type _aligned;
  nState m_state _aligned;
  nStateVector m_refVector _aligned;
  bool m_refIsSet _aligned;
  int* m_measurement _aligned;
  double m_residual _aligned;
  double m_errResidual _aligned;
  double m_errMeasure _aligned;
  TrackProjectionMatrix m_projectionMatrix _aligned;

  nNode () = default;
  nNode (const nNode& node) = default;
  nNode (const FitNode& node);
};

struct nFitNode : public nNode {
  TrackVector m_transportVector _aligned;
  TrackSymMatrix m_noiseMatrix _aligned;
  nFitParameters m_forwardFit _aligned;
  nFitParameters m_backwardFit _aligned;
  double m_refResidual _aligned;
  bool m_isLast _aligned = false;
  
  // Parameters to allow the conversion back
  // Eventually will be removed
  int m_parent_nTrackParameters;
  CachedBool m_hasInfoUpstream [2];

  nFitNode () = default;
  nFitNode (const nFitNode& node) = default;
  nFitNode (const FitNode& node);

  template<class R, class S, class T,
    class U = typename std::conditional<std::is_same<T, Fit::Covariance>::value, nTrackSymMatrix, nTrackVector>::type>
  const U& getState () const;
  template<class R, class S, class T,
    class U = typename std::conditional<std::is_same<T, Fit::Covariance>::value, nTrackSymMatrix, nTrackVector>::type>
  U& getState ();

  template<class R>
  const nFitParameters& getFit () const;
  template<class R>
  nFitParameters& getFit ();

  template<class R>
  const double& getChi2 () const {
    return *(getFit<R>().m_states.m_chi2);
  }
  template<class R>
  double& getChi2 () {
    return *(getFit<R>().m_states.m_chi2);
  }
};

struct nTrack {
  std::vector<nFitNode> m_nodes _aligned;

  // Active measurements from this node
  unsigned int m_forwardUpstream _aligned;
  unsigned int m_backwardUpstream _aligned;

  // Chi square of track
  double m_forwardFitChi2 _aligned = 0.0;
  double m_backwardFitChi2 _aligned = 0.0;
  int m_ndof = 0;
  int m_trackNumber;

  // Outlier
  int outlier = -1;

  // Initial covariances
  TrackSymMatrix m_initialForwardCovariance;
  TrackSymMatrix m_initialBackwardCovariance;

  nTrack () = default;
  nTrack (const nTrack& track) = default;
  nTrack (const std::vector<FitNode>& track, int trackNumber = -1);
};

struct SchItem {
  std::vector<nFitNode>* track;
  std::vector<nFitNode>::iterator node;
  std::vector<nFitNode>::iterator prevnode;
  int slot;

  SchItem () = default;
  SchItem (const SchItem& copy) = default;
  SchItem (
    std::vector<nFitNode>* track,
    const unsigned int& nodeIndex,
    const unsigned int& prevnodeIndex,
    const int& slot
  ) : track(track), slot(slot) {
    node = track->begin() + nodeIndex;
    prevnode = track->begin() + prevnodeIndex;
  }
};
