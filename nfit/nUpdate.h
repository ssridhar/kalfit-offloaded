#pragma once

#include "nTypes.h"
#include "../oldfit/Similarity.h"
#include "nSimilarity.h"

template<class D>
void nupdate (
  nFitNode& node
) {
  if (node.m_type != HitOnTrack) {
    node.getState<D, Fit::Updated, Fit::StateVector>().setBasePointer(node.getState<D, Fit::Predicted, Fit::StateVector>());
    node.getState<D, Fit::Updated, Fit::Covariance>().setBasePointer(node.getState<D, Fit::Predicted, Fit::Covariance>());
  } else {
    node.getState<D, Fit::Updated, Fit::StateVector>().copy(node.getState<D, Fit::Predicted, Fit::StateVector>());
    node.getState<D, Fit::Updated, Fit::Covariance>().copy(node.getState<D, Fit::Predicted, Fit::Covariance>());

    math_update_nonvec (
      node.getState<D, Fit::Updated, Fit::StateVector>(),
      node.getState<D, Fit::Updated, Fit::Covariance>(),
      node.getChi2<D>(),
      node.m_refVector.m_parameters.fArray,
      node.m_projectionMatrix.fArray,
      node.m_refResidual,
      node.m_errMeasure * node.m_errMeasure
    );
  }
}

template<unsigned W>
void nupdate_vec (
  std::array<SchItem, W>& n,
  double_ptr_64_const ps,
  double_ptr_64_const pc,
  double_ptr_64 us,
  double_ptr_64 uc,
  double_ptr_64 chi2
) {
  // #define _Xref(_x, _y) _x.node->m_refVector.m_parameters.fArray[_y]
  // #define _H(_x, _y)    _x.node->m_projectionMatrix.fArray[_y]
  // #define _refRes(_x)   _x.node->m_refResidual
  // #define _errMeas(_x)  _x.node->m_errMeasure * _x.node->m_errMeasure

  // const std::array<int, W> mask {
  //   n[0].node->m_type == HitOnTrack,
  //   n[1].node->m_type == HitOnTrack,
  //   n[2].node->m_type == HitOnTrack,
  //   n[3].node->m_type == HitOnTrack,
  //   n[4].node->m_type == HitOnTrack,
  //   n[5].node->m_type == HitOnTrack,
  //   n[6].node->m_type == HitOnTrack,
  //   n[7].node->m_type == HitOnTrack
  // };

  // const double Xref [W * 5] _aligned = {
  //   _Xref(n[0], 0), _Xref(n[1], 0), _Xref(n[2], 0), _Xref(n[3], 0), _Xref(n[4], 0), _Xref(n[5], 0), _Xref(n[6], 0), _Xref(n[7], 0),
  //   _Xref(n[0], 1), _Xref(n[1], 1), _Xref(n[2], 1), _Xref(n[3], 1), _Xref(n[4], 1), _Xref(n[5], 1), _Xref(n[6], 1), _Xref(n[7], 1),
  //   _Xref(n[0], 2), _Xref(n[1], 2), _Xref(n[2], 2), _Xref(n[3], 2), _Xref(n[4], 2), _Xref(n[5], 2), _Xref(n[6], 2), _Xref(n[7], 2),
  //   _Xref(n[0], 3), _Xref(n[1], 3), _Xref(n[2], 3), _Xref(n[3], 3), _Xref(n[4], 3), _Xref(n[5], 3), _Xref(n[6], 3), _Xref(n[7], 3),
  //   _Xref(n[0], 4), _Xref(n[1], 4), _Xref(n[2], 4), _Xref(n[3], 4), _Xref(n[4], 4), _Xref(n[5], 4), _Xref(n[6], 4), _Xref(n[7], 4)
  // };
  // const double H [W * 5] _aligned = {
  //   _H(n[0], 0), _H(n[1], 0), _H(n[2], 0), _H(n[3], 0), _H(n[4], 0), _H(n[5], 0), _H(n[6], 0), _H(n[7], 0),
  //   _H(n[0], 1), _H(n[1], 1), _H(n[2], 1), _H(n[3], 1), _H(n[4], 1), _H(n[5], 1), _H(n[6], 1), _H(n[7], 1),
  //   _H(n[0], 2), _H(n[1], 2), _H(n[2], 2), _H(n[3], 2), _H(n[4], 2), _H(n[5], 2), _H(n[6], 2), _H(n[7], 2),
  //   _H(n[0], 3), _H(n[1], 3), _H(n[2], 3), _H(n[3], 3), _H(n[4], 3), _H(n[5], 3), _H(n[6], 3), _H(n[7], 3),
  //   _H(n[0], 4), _H(n[1], 4), _H(n[2], 4), _H(n[3], 4), _H(n[4], 4), _H(n[5], 4), _H(n[6], 4), _H(n[7], 4)
  // };
  // const double refResidual [W] _aligned = {
  //   _refRes(n[0]), _refRes(n[1]), _refRes(n[2]), _refRes(n[3]), _refRes(n[4]), _refRes(n[5]), _refRes(n[6]), _refRes(n[7]),
  // };
  // const double errorMeas2 [W] _aligned = {
  //   _errMeas(n[0]), _errMeas(n[1]), _errMeas(n[2]), _errMeas(n[3]), _errMeas(n[4]), _errMeas(n[5]), _errMeas(n[6]), _errMeas(n[7]),
  // };

  std::array<double, 5*W> Xref _aligned;
  std::array<double, 5*W> H _aligned;
  std::array<double, W> refResidual _aligned;
  std::array<double, W> errorMeas2 _aligned;
  std::array<int, W> mask _aligned;

  for (int i=0; i<5; ++i) {
    for (int j=0; j<W; ++j) {
      Xref[i*W + j] = n[j].node->m_refVector.m_parameters.fArray[i];
    }
  }

  for (int i=0; i<5; ++i) {
    for (int j=0; j<W; ++j) {
      H[i*W + j] = n[j].node->m_projectionMatrix.fArray[i];
    }
  }

  for (int i=0; i<W; ++i) {
    refResidual[i] = n[i].node->m_refResidual;
    errorMeas2[i] = n[i].node->m_errMeasure * n[i].node->m_errMeasure;
    mask[i] = n[i].node->m_type == HitOnTrack;
  }

  FitMath<W>::update (
    us,
    uc,
    chi2,
    ps,
    pc,
    Xref,
    H,
    refResidual,
    errorMeas2,
    mask
  );
}
