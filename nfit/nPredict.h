#pragma once

#include "nTypes.h"
#include "nSimilarity.h"
#include "../oldfit/Predict.h"

template<class T>
void ntransportCovariance (
  const TrackMatrix& tm,
  const T& uc,
  nTrackSymMatrix& pc
) {
  bool isLine = tm(0,4)==0;
  if (!isLine) {
    math_similarity_5_5_nonvec(tm, uc, pc);
  } else {
    pc.copy(uc);
    
    if (tm(0,2) != 0 or tm(1,3) != 0) {
      pc(0,0) += 2 * uc(2,0) * tm(0,2) + uc(2,2) * tm(0,2) * tm(0,2);
      pc(2,0) += uc(2,2) * tm(0,2);
      pc(1,1) += 2 * uc(3,1) * tm(1,3) + uc(3,3) * tm(1,3) * tm(1,3);
      pc(3,1) += uc(3,3) * tm(1,3);
      pc(1,0) += uc(2,1) * tm(0,2) + uc(3,0) * tm(1,3) + uc(3,2) * tm(0,2) * tm(1,3);
      pc(2,1) += uc(3,2) * tm(1,3);
      pc(3,0) += uc(3,2) * tm(0,2);
    }
  }
}

template<class T>
void npredictInitialise (
  nFitNode& node,
  const TrackSymMatrix& covariance
) {
  node.getState<T, Fit::Predicted, Fit::StateVector>().copy(node.m_refVector.m_parameters);
  node.getState<T, Fit::Predicted, Fit::Covariance>().copy(covariance);
}

template<class T, bool U>
void npredict (
  nFitNode& node,
  const nFitNode& prevnode
);

template<class T>
struct npredict_vec {
  template<unsigned W>
  static void op (
    std::array<SchItem, W>& n,
    double_ptr_64_const uc,
    double_ptr_64 pc
  );
};

// Vectorised predicts that assume data is already prepared, aligned and so on

template<>
struct npredict_vec<Fit::Forward> {
  template<unsigned W>
  static void op (
    std::array<SchItem, W>& n,
    double_ptr_64_const uc,
    double_ptr_64 pc
  ) {
    // I don't know how to do this anymore
    // #define _tm(_x, _y) _x.node->m_forwardFit.m_transportMatrix.fArray[_y]
    // const double tm [W * 25] _aligned {
    //   _tm(n[0], 0), _tm(n[1], 0), _tm(n[2], 0), _tm(n[3], 0), _tm(n[4], 0), _tm(n[5], 0), _tm(n[6], 0), _tm(n[7], 0),
    //   _tm(n[0], 1), _tm(n[1], 1), _tm(n[2], 1), _tm(n[3], 1), _tm(n[4], 1), _tm(n[5], 1), _tm(n[6], 1), _tm(n[7], 1),
    //   _tm(n[0], 2), _tm(n[1], 2), _tm(n[2], 2), _tm(n[3], 2), _tm(n[4], 2), _tm(n[5], 2), _tm(n[6], 2), _tm(n[7], 2),
    //   _tm(n[0], 3), _tm(n[1], 3), _tm(n[2], 3), _tm(n[3], 3), _tm(n[4], 3), _tm(n[5], 3), _tm(n[6], 3), _tm(n[7], 3),
    //   _tm(n[0], 4), _tm(n[1], 4), _tm(n[2], 4), _tm(n[3], 4), _tm(n[4], 4), _tm(n[5], 4), _tm(n[6], 4), _tm(n[7], 4),
    //   _tm(n[0], 5), _tm(n[1], 5), _tm(n[2], 5), _tm(n[3], 5), _tm(n[4], 5), _tm(n[5], 5), _tm(n[6], 5), _tm(n[7], 5),
    //   _tm(n[0], 6), _tm(n[1], 6), _tm(n[2], 6), _tm(n[3], 6), _tm(n[4], 6), _tm(n[5], 6), _tm(n[6], 6), _tm(n[7], 6),
    //   _tm(n[0], 7), _tm(n[1], 7), _tm(n[2], 7), _tm(n[3], 7), _tm(n[4], 7), _tm(n[5], 7), _tm(n[6], 7), _tm(n[7], 7),
    //   _tm(n[0], 8), _tm(n[1], 8), _tm(n[2], 8), _tm(n[3], 8), _tm(n[4], 8), _tm(n[5], 8), _tm(n[6], 8), _tm(n[7], 8),
    //   _tm(n[0], 9), _tm(n[1], 9), _tm(n[2], 9), _tm(n[3], 9), _tm(n[4], 9), _tm(n[5], 9), _tm(n[6], 9), _tm(n[7], 9),
    //   _tm(n[0], 10), _tm(n[1], 10), _tm(n[2], 10), _tm(n[3], 10), _tm(n[4], 10), _tm(n[5], 10), _tm(n[6], 10), _tm(n[7], 10),
    //   _tm(n[0], 11), _tm(n[1], 11), _tm(n[2], 11), _tm(n[3], 11), _tm(n[4], 11), _tm(n[5], 11), _tm(n[6], 11), _tm(n[7], 11),
    //   _tm(n[0], 12), _tm(n[1], 12), _tm(n[2], 12), _tm(n[3], 12), _tm(n[4], 12), _tm(n[5], 12), _tm(n[6], 12), _tm(n[7], 12),
    //   _tm(n[0], 13), _tm(n[1], 13), _tm(n[2], 13), _tm(n[3], 13), _tm(n[4], 13), _tm(n[5], 13), _tm(n[6], 13), _tm(n[7], 13),
    //   _tm(n[0], 14), _tm(n[1], 14), _tm(n[2], 14), _tm(n[3], 14), _tm(n[4], 14), _tm(n[5], 14), _tm(n[6], 14), _tm(n[7], 14),
    //   _tm(n[0], 15), _tm(n[1], 15), _tm(n[2], 15), _tm(n[3], 15), _tm(n[4], 15), _tm(n[5], 15), _tm(n[6], 15), _tm(n[7], 15),
    //   _tm(n[0], 16), _tm(n[1], 16), _tm(n[2], 16), _tm(n[3], 16), _tm(n[4], 16), _tm(n[5], 16), _tm(n[6], 16), _tm(n[7], 16),
    //   _tm(n[0], 17), _tm(n[1], 17), _tm(n[2], 17), _tm(n[3], 17), _tm(n[4], 17), _tm(n[5], 17), _tm(n[6], 17), _tm(n[7], 17),
    //   _tm(n[0], 18), _tm(n[1], 18), _tm(n[2], 18), _tm(n[3], 18), _tm(n[4], 18), _tm(n[5], 18), _tm(n[6], 18), _tm(n[7], 18),
    //   _tm(n[0], 19), _tm(n[1], 19), _tm(n[2], 19), _tm(n[3], 19), _tm(n[4], 19), _tm(n[5], 19), _tm(n[6], 19), _tm(n[7], 19),
    //   _tm(n[0], 20), _tm(n[1], 20), _tm(n[2], 20), _tm(n[3], 20), _tm(n[4], 20), _tm(n[5], 20), _tm(n[6], 20), _tm(n[7], 20),
    //   _tm(n[0], 21), _tm(n[1], 21), _tm(n[2], 21), _tm(n[3], 21), _tm(n[4], 21), _tm(n[5], 21), _tm(n[6], 21), _tm(n[7], 21),
    //   _tm(n[0], 22), _tm(n[1], 22), _tm(n[2], 22), _tm(n[3], 22), _tm(n[4], 22), _tm(n[5], 22), _tm(n[6], 22), _tm(n[7], 22),
    //   _tm(n[0], 23), _tm(n[1], 23), _tm(n[2], 23), _tm(n[3], 23), _tm(n[4], 23), _tm(n[5], 23), _tm(n[6], 23), _tm(n[7], 23),
    //   _tm(n[0], 24), _tm(n[1], 24), _tm(n[2], 24), _tm(n[3], 24), _tm(n[4], 24), _tm(n[5], 24), _tm(n[6], 24), _tm(n[7], 24)
    // };
    
    // I'd like to do this statically, initialized with braces
    // (perhaps compiler will know what I want anyway)
    double tm [25 * W] _aligned;
    for (int i=0; i<25; ++i) {
      for (int j=0; j<W; ++j) {
        tm[i*W + j] = n[j].node->m_forwardFit.m_transportMatrix.fArray[i];
      }
    }

    FitMath<W>::similarity_5_5 (
      tm, // nodeFit.m_transportMatrix.fArray,
      uc, // prevnode.m_forwardFit.m_updatedState.m_covariance.fArray,
      pc  // nodeFit.m_predictedState.m_covariance.fArray
    );

    for (int i=0; i<W; ++i) {
      // update state
      n[i].node->template getState<Fit::Forward, Fit::Predicted, Fit::StateVector>().copy(n[i].node->m_transportVector);
      for (int j=0; j<5; ++j) {
        for (int k=0; k<5; ++k) {
          n[i].node->template getState<Fit::Forward, Fit::Predicted, Fit::StateVector>()[j] += 
            n[i].node->template getFit<Fit::Forward>().m_transportMatrix[5*j + k] *
            n[i].prevnode->template getState<Fit::Forward, Fit::Updated, Fit::StateVector>()[k];
        }
      }

      // add the noise matrix
      n[i].node->template getState<Fit::Forward, Fit::Predicted, Fit::Covariance>() += n[i].node->m_noiseMatrix;
    }
  }
};

template<>
struct npredict_vec<Fit::Backward> {
  template<unsigned W>
  static void op (
    std::array<SchItem, W>& n,
    double_ptr_64_const uc,
    double_ptr_64 pc
  ) {
    // #define _btm(_x, _y) _x.prevnode->m_backwardFit.m_transportMatrix.fArray[_y]
    // #define _nm(_x, _y) _x.prevnode->m_noiseMatrix.fArray[_y]

    // const double tm [VECTOR_WIDTH * 25] _aligned {
    //   _btm(n[0], 0), _btm(n[1], 0), _btm(n[2], 0), _btm(n[3], 0), _btm(n[4], 0), _btm(n[5], 0), _btm(n[6], 0), _btm(n[7], 0),
    //   _btm(n[0], 1), _btm(n[1], 1), _btm(n[2], 1), _btm(n[3], 1), _btm(n[4], 1), _btm(n[5], 1), _btm(n[6], 1), _btm(n[7], 1),
    //   _btm(n[0], 2), _btm(n[1], 2), _btm(n[2], 2), _btm(n[3], 2), _btm(n[4], 2), _btm(n[5], 2), _btm(n[6], 2), _btm(n[7], 2),
    //   _btm(n[0], 3), _btm(n[1], 3), _btm(n[2], 3), _btm(n[3], 3), _btm(n[4], 3), _btm(n[5], 3), _btm(n[6], 3), _btm(n[7], 3),
    //   _btm(n[0], 4), _btm(n[1], 4), _btm(n[2], 4), _btm(n[3], 4), _btm(n[4], 4), _btm(n[5], 4), _btm(n[6], 4), _btm(n[7], 4),
    //   _btm(n[0], 5), _btm(n[1], 5), _btm(n[2], 5), _btm(n[3], 5), _btm(n[4], 5), _btm(n[5], 5), _btm(n[6], 5), _btm(n[7], 5),
    //   _btm(n[0], 6), _btm(n[1], 6), _btm(n[2], 6), _btm(n[3], 6), _btm(n[4], 6), _btm(n[5], 6), _btm(n[6], 6), _btm(n[7], 6),
    //   _btm(n[0], 7), _btm(n[1], 7), _btm(n[2], 7), _btm(n[3], 7), _btm(n[4], 7), _btm(n[5], 7), _btm(n[6], 7), _btm(n[7], 7),
    //   _btm(n[0], 8), _btm(n[1], 8), _btm(n[2], 8), _btm(n[3], 8), _btm(n[4], 8), _btm(n[5], 8), _btm(n[6], 8), _btm(n[7], 8),
    //   _btm(n[0], 9), _btm(n[1], 9), _btm(n[2], 9), _btm(n[3], 9), _btm(n[4], 9), _btm(n[5], 9), _btm(n[6], 9), _btm(n[7], 9),
    //   _btm(n[0], 10), _btm(n[1], 10), _btm(n[2], 10), _btm(n[3], 10), _btm(n[4], 10), _btm(n[5], 10), _btm(n[6], 10), _btm(n[7], 10),
    //   _btm(n[0], 11), _btm(n[1], 11), _btm(n[2], 11), _btm(n[3], 11), _btm(n[4], 11), _btm(n[5], 11), _btm(n[6], 11), _btm(n[7], 11),
    //   _btm(n[0], 12), _btm(n[1], 12), _btm(n[2], 12), _btm(n[3], 12), _btm(n[4], 12), _btm(n[5], 12), _btm(n[6], 12), _btm(n[7], 12),
    //   _btm(n[0], 13), _btm(n[1], 13), _btm(n[2], 13), _btm(n[3], 13), _btm(n[4], 13), _btm(n[5], 13), _btm(n[6], 13), _btm(n[7], 13),
    //   _btm(n[0], 14), _btm(n[1], 14), _btm(n[2], 14), _btm(n[3], 14), _btm(n[4], 14), _btm(n[5], 14), _btm(n[6], 14), _btm(n[7], 14),
    //   _btm(n[0], 15), _btm(n[1], 15), _btm(n[2], 15), _btm(n[3], 15), _btm(n[4], 15), _btm(n[5], 15), _btm(n[6], 15), _btm(n[7], 15),
    //   _btm(n[0], 16), _btm(n[1], 16), _btm(n[2], 16), _btm(n[3], 16), _btm(n[4], 16), _btm(n[5], 16), _btm(n[6], 16), _btm(n[7], 16),
    //   _btm(n[0], 17), _btm(n[1], 17), _btm(n[2], 17), _btm(n[3], 17), _btm(n[4], 17), _btm(n[5], 17), _btm(n[6], 17), _btm(n[7], 17),
    //   _btm(n[0], 18), _btm(n[1], 18), _btm(n[2], 18), _btm(n[3], 18), _btm(n[4], 18), _btm(n[5], 18), _btm(n[6], 18), _btm(n[7], 18),
    //   _btm(n[0], 19), _btm(n[1], 19), _btm(n[2], 19), _btm(n[3], 19), _btm(n[4], 19), _btm(n[5], 19), _btm(n[6], 19), _btm(n[7], 19),
    //   _btm(n[0], 20), _btm(n[1], 20), _btm(n[2], 20), _btm(n[3], 20), _btm(n[4], 20), _btm(n[5], 20), _btm(n[6], 20), _btm(n[7], 20),
    //   _btm(n[0], 21), _btm(n[1], 21), _btm(n[2], 21), _btm(n[3], 21), _btm(n[4], 21), _btm(n[5], 21), _btm(n[6], 21), _btm(n[7], 21),
    //   _btm(n[0], 22), _btm(n[1], 22), _btm(n[2], 22), _btm(n[3], 22), _btm(n[4], 22), _btm(n[5], 22), _btm(n[6], 22), _btm(n[7], 22),
    //   _btm(n[0], 23), _btm(n[1], 23), _btm(n[2], 23), _btm(n[3], 23), _btm(n[4], 23), _btm(n[5], 23), _btm(n[6], 23), _btm(n[7], 23),
    //   _btm(n[0], 24), _btm(n[1], 24), _btm(n[2], 24), _btm(n[3], 24), _btm(n[4], 24), _btm(n[5], 24), _btm(n[6], 24), _btm(n[7], 24)
    // };

    // double nm [VECTOR_WIDTH * 15] _aligned {
    //   _nm(n[0], 0), _nm(n[1], 0), _nm(n[2], 0), _nm(n[3], 0), _nm(n[4], 0), _nm(n[5], 0), _nm(n[6], 0), _nm(n[7], 0),
    //   _nm(n[0], 1), _nm(n[1], 1), _nm(n[2], 1), _nm(n[3], 1), _nm(n[4], 1), _nm(n[5], 1), _nm(n[6], 1), _nm(n[7], 1),
    //   _nm(n[0], 2), _nm(n[1], 2), _nm(n[2], 2), _nm(n[3], 2), _nm(n[4], 2), _nm(n[5], 2), _nm(n[6], 2), _nm(n[7], 2),
    //   _nm(n[0], 3), _nm(n[1], 3), _nm(n[2], 3), _nm(n[3], 3), _nm(n[4], 3), _nm(n[5], 3), _nm(n[6], 3), _nm(n[7], 3),
    //   _nm(n[0], 4), _nm(n[1], 4), _nm(n[2], 4), _nm(n[3], 4), _nm(n[4], 4), _nm(n[5], 4), _nm(n[6], 4), _nm(n[7], 4),
    //   _nm(n[0], 5), _nm(n[1], 5), _nm(n[2], 5), _nm(n[3], 5), _nm(n[4], 5), _nm(n[5], 5), _nm(n[6], 5), _nm(n[7], 5),
    //   _nm(n[0], 6), _nm(n[1], 6), _nm(n[2], 6), _nm(n[3], 6), _nm(n[4], 6), _nm(n[5], 6), _nm(n[6], 6), _nm(n[7], 6),
    //   _nm(n[0], 7), _nm(n[1], 7), _nm(n[2], 7), _nm(n[3], 7), _nm(n[4], 7), _nm(n[5], 7), _nm(n[6], 7), _nm(n[7], 7),
    //   _nm(n[0], 8), _nm(n[1], 8), _nm(n[2], 8), _nm(n[3], 8), _nm(n[4], 8), _nm(n[5], 8), _nm(n[6], 8), _nm(n[7], 8),
    //   _nm(n[0], 9), _nm(n[1], 9), _nm(n[2], 9), _nm(n[3], 9), _nm(n[4], 9), _nm(n[5], 9), _nm(n[6], 9), _nm(n[7], 9),
    //   _nm(n[0], 10), _nm(n[1], 10), _nm(n[2], 10), _nm(n[3], 10), _nm(n[4], 10), _nm(n[5], 10), _nm(n[6], 10), _nm(n[7], 10),
    //   _nm(n[0], 11), _nm(n[1], 11), _nm(n[2], 11), _nm(n[3], 11), _nm(n[4], 11), _nm(n[5], 11), _nm(n[6], 11), _nm(n[7], 11),
    //   _nm(n[0], 12), _nm(n[1], 12), _nm(n[2], 12), _nm(n[3], 12), _nm(n[4], 12), _nm(n[5], 12), _nm(n[6], 12), _nm(n[7], 12),
    //   _nm(n[0], 13), _nm(n[1], 13), _nm(n[2], 13), _nm(n[3], 13), _nm(n[4], 13), _nm(n[5], 13), _nm(n[6], 13), _nm(n[7], 13),
    //   _nm(n[0], 14), _nm(n[1], 14), _nm(n[2], 14), _nm(n[3], 14), _nm(n[4], 14), _nm(n[5], 14), _nm(n[6], 14), _nm(n[7], 14)
    // };

    // I'd like to do this statically, initialized with braces
    // (perhaps compiler will know what I want anyway)
    double tm [25 * W] _aligned;
    double nm [15 * W] _aligned;

    for (int i=0; i<25; ++i) {
      for (int j=0; j<W; ++j) {
        tm[i*W + j] = n[j].prevnode->m_backwardFit.m_transportMatrix.fArray[i];
      }
    }

    for (int i=0; i<15; ++i) {
      for (int j=0; j<W; ++j) {
        nm[i*W + j] = n[j].prevnode->m_noiseMatrix.fArray[i];
      }
    }

    FitMath<W>::similarity_5_5 (
      tm, // nodeFit.m_transportMatrix.fArray,
      uc, // prevnode.m_forwardFit.m_updatedState.m_covariance.fArray,
      nm, // prevnode.m_noiseMatrix.fArray,
      pc  // nodeFit.m_predictedState.m_covariance.fArray
    );

    for (int i=0; i<W; ++i) {
      // update state
      TrackVector temp_sub;
      for (int j=0; j<5; ++j) {
        temp_sub[j] = n[i].prevnode->template getState<Fit::Backward, Fit::Updated, Fit::StateVector>()[j] - n[i].prevnode->m_transportVector[j];
      };

      for (int j=0; j<5; ++j) {
        n[i].node->template getState<Fit::Backward, Fit::Predicted, Fit::StateVector>()[j] =
          n[i].prevnode->template getFit<Fit::Backward>().m_transportMatrix[5*j] * temp_sub[0];
        for (int k=1; k<5; ++k) {
          n[i].node->template getState<Fit::Backward, Fit::Predicted, Fit::StateVector>()[j] +=
            n[i].prevnode->template getFit<Fit::Backward>().m_transportMatrix[5*j + k] * temp_sub[k];
        }
      }
    }
  }
};
