#include "nTypes.h"

// Getters for states

// ---------------
// get methods
// ---------------

template<>
const nFitParameters& nFitNode::getFit<Fit::Backward> () const {
  return m_backwardFit;
}

template<>
const nFitParameters& nFitNode::getFit<Fit::Forward> () const {
  return m_forwardFit;
}

template<>
const nTrackVector& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::StateVector> () const {
  return m_forwardFit.m_states.m_predictedState;
}

template<>
const nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::Covariance> () const {
  return m_forwardFit.m_states.m_predictedCovariance;
}

template<>
const nTrackVector& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::StateVector> () const {
  return m_forwardFit.m_states.m_updatedState;
}

template<>
const nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::Covariance> () const {
  return m_forwardFit.m_states.m_updatedCovariance;
}

template<>
const nTrackVector& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::StateVector> () const {
  return m_backwardFit.m_states.m_predictedState;
}

template<>
const nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::Covariance> () const {
  return m_backwardFit.m_states.m_predictedCovariance;
}

template<>
const nTrackVector& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::StateVector> () const {
  return m_backwardFit.m_states.m_updatedState;
}

template<>
const nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::Covariance> () const {
  return m_backwardFit.m_states.m_updatedCovariance;
}

// -------------------------
// ref getters for the above
// -------------------------

template<>
nFitParameters& nFitNode::getFit<Fit::Forward> () {
  return m_forwardFit;
}

template<>
nFitParameters& nFitNode::getFit<Fit::Backward> () {
  return m_backwardFit;
}

template<>
nTrackVector& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::StateVector> () {
  return m_forwardFit.m_states.m_predictedState;
}

template<>
nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Predicted, Fit::Covariance> () {
  return m_forwardFit.m_states.m_predictedCovariance;
}

template<>
nTrackVector& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::StateVector> () {
  return m_forwardFit.m_states.m_updatedState;
}

template<>
nTrackSymMatrix& nFitNode::getState<Fit::Forward, Fit::Updated, Fit::Covariance> () {
  return m_forwardFit.m_states.m_updatedCovariance;
}

template<>
nTrackVector& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::StateVector> () {
  return m_backwardFit.m_states.m_predictedState;
}

template<>
nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Predicted, Fit::Covariance> () {
  return m_backwardFit.m_states.m_predictedCovariance;
}

template<>
nTrackVector& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::StateVector> () {
  return m_backwardFit.m_states.m_updatedState;
}

template<>
nTrackSymMatrix& nFitNode::getState<Fit::Backward, Fit::Updated, Fit::Covariance> () {
  return m_backwardFit.m_states.m_updatedCovariance;
}
