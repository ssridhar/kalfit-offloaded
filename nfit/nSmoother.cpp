#include "nSmoother.h"

// void nsmoother (
//   nFitNode& node,
//   const bool hasInfoUpstreamForward,
//   const bool hasInfoUpstreamBackward
// ) {
//   if (!hasInfoUpstreamForward) {
//     // node.m_state = node.getState<Fit::Backward, Fit::Updated>();
//     node.m_state.m_stateVector = node.getState<Fit::Backward, Fit::Updated, Fit::StateVector>();
//     node.m_state.m_covariance = node.getState<Fit::Backward, Fit::Updated, Fit::Covariance>();
//   } else if (!hasInfoUpstreamBackward) {
//     // node.m_state = node.getState<Fit::Forward, Fit::Updated>();
//     node.m_state.m_stateVector = node.getState<Fit::Forward, Fit::Updated, Fit::StateVector>();
//     node.m_state.m_covariance = node.getState<Fit::Forward, Fit::Updated, Fit::Covariance>();
//   } else {
//     const bool condition =
//       node.getState<Fit::Backward, Fit::Predicted, Fit::Covariance>()(0,0) > node.getState<Fit::Forward, Fit::Predicted, Fit::Covariance>()(0,0);
    
//     // const nState& s0 = condition ? node.getState<Fit::Backward, Fit::Updated>() : node.getState<Fit::Forward, Fit::Updated>();
//     const TrackVector& s0_stateVector = condition ?
//       node.getState<Fit::Backward, Fit::Updated, Fit::StateVector>() :
//       node.getState<Fit::Forward, Fit::Updated, Fit::StateVector>();

//     const TrackSymMatrix& s0_covariance = condition ?
//       node.getState<Fit::Backward, Fit::Updated, Fit::Covariance>() :
//       node.getState<Fit::Forward, Fit::Updated, Fit::Covariance>();
  
//     const TrackVector& s1_stateVector = condition ?
//       node.getState<Fit::Forward, Fit::Predicted, Fit::StateVector>() :
//       node.getState<Fit::Backward, Fit::Predicted, Fit::StateVector>();

//     const TrackSymMatrix& s1_covariance = condition ? 
//       node.m_forwardFit.m_predictedState.m_covariance : 
//       node.m_backwardFit.m_predictedState.m_covariance;

//     // const nState& s1 = condition ? node.m_forwardFit.m_predictedState : node.m_backwardFit.m_predictedState;

//     math_average (
//       s0_stateVector.fArray,
//       s0_covariance.fArray,
//       s1_stateVector.fArray,
//       s1_covariance.fArray,
//       node.m_state.m_stateVector.fArray,
//       node.m_state.m_covariance.fArray
//     );
//   }
  
//   double value {0.0}, error {0.0};
//   if (node.m_measurement != 0) {
//     const TrackProjectionMatrix& H  = node.m_projectionMatrix;
//     double HCH;
//     math_similarity_5_1 (
//       node.m_state.m_covariance.fArray,
//       H.fArray,
//       &HCH);
    
//     const TrackVector& refX = node.m_refVector.m_parameters;
//     const double V = node.m_errMeasure * node.m_errMeasure;
//     const double sign = node.m_type == HitOnTrack ? -1 : 1;
//     value = node.m_refResidual + (H * (refX - node.m_state.m_stateVector));
//     error = V + sign * HCH;
//   }
//   node.m_residual = value;
//   node.m_errResidual = error;
// }
