#pragma once

#include <array>
#include "nStore.h"
#include "nTypes.h"

template<unsigned W>
struct FitMath {
  static void similarity_5_5 (
    double_ptr_64_const tm,
    double_ptr_64_const uc,
    double_ptr_64 pc
  ) {
    double _aligned v [5 * W];

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = uc[ _2D(0)]*tm[_2D(0)]+uc[ _2D(1)]*tm[_2D(1)]+uc[ _2D(3)]*tm[_2D(2)]+uc[ _2D(6)]*tm[_2D(3)]+uc[_2D(10)]*tm[_2D(4)];
      v[_2D(1)] = uc[ _2D(1)]*tm[_2D(0)]+uc[ _2D(2)]*tm[_2D(1)]+uc[ _2D(4)]*tm[_2D(2)]+uc[ _2D(7)]*tm[_2D(3)]+uc[_2D(11)]*tm[_2D(4)];
      v[_2D(2)] = uc[ _2D(3)]*tm[_2D(0)]+uc[ _2D(4)]*tm[_2D(1)]+uc[ _2D(5)]*tm[_2D(2)]+uc[ _2D(8)]*tm[_2D(3)]+uc[_2D(12)]*tm[_2D(4)];
      v[_2D(3)] = uc[ _2D(6)]*tm[_2D(0)]+uc[ _2D(7)]*tm[_2D(1)]+uc[ _2D(8)]*tm[_2D(2)]+uc[ _2D(9)]*tm[_2D(3)]+uc[_2D(13)]*tm[_2D(4)];
      v[_2D(4)] = uc[_2D(10)]*tm[_2D(0)]+uc[_2D(11)]*tm[_2D(1)]+uc[_2D(12)]*tm[_2D(2)]+uc[_2D(13)]*tm[_2D(3)]+uc[_2D(14)]*tm[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      pc[ _2D(0)] = tm[ _2D(0)]*v[_2D(0)] + tm[ _2D(1)]*v[_2D(1)] + tm[ _2D(2)]*v[_2D(2)] + tm[ _2D(3)]*v[_2D(3)] + tm[ _2D(4)]*v[_2D(4)];
      pc[ _2D(1)] = tm[ _2D(5)]*v[_2D(0)] + tm[ _2D(6)]*v[_2D(1)] + tm[ _2D(7)]*v[_2D(2)] + tm[ _2D(8)]*v[_2D(3)] + tm[ _2D(9)]*v[_2D(4)];
      pc[ _2D(3)] = tm[_2D(10)]*v[_2D(0)] + tm[_2D(11)]*v[_2D(1)] + tm[_2D(12)]*v[_2D(2)] + tm[_2D(13)]*v[_2D(3)] + tm[_2D(14)]*v[_2D(4)];
      pc[ _2D(6)] = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(10)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] =  uc[ _2D(0)]*tm[_2D(5)]+uc[ _2D(1)]*tm[_2D(6)]+uc[ _2D(3)]*tm[_2D(7)]+uc[ _2D(6)]*tm[_2D(8)]+uc[_2D(10)]*tm[_2D(9)];
      v[_2D(1)] =  uc[ _2D(1)]*tm[_2D(5)]+uc[ _2D(2)]*tm[_2D(6)]+uc[ _2D(4)]*tm[_2D(7)]+uc[ _2D(7)]*tm[_2D(8)]+uc[_2D(11)]*tm[_2D(9)];
      v[_2D(2)] =  uc[ _2D(3)]*tm[_2D(5)]+uc[ _2D(4)]*tm[_2D(6)]+uc[ _2D(5)]*tm[_2D(7)]+uc[ _2D(8)]*tm[_2D(8)]+uc[_2D(12)]*tm[_2D(9)];
      v[_2D(3)] =  uc[ _2D(6)]*tm[_2D(5)]+uc[ _2D(7)]*tm[_2D(6)]+uc[ _2D(8)]*tm[_2D(7)]+uc[ _2D(9)]*tm[_2D(8)]+uc[_2D(13)]*tm[_2D(9)];
      v[_2D(4)] =  uc[_2D(10)]*tm[_2D(5)]+uc[_2D(11)]*tm[_2D(6)]+uc[_2D(12)]*tm[_2D(7)]+uc[_2D(13)]*tm[_2D(8)]+uc[_2D(14)]*tm[_2D(9)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(2)]  = tm[ _2D(5)]*v[_2D(0)] + tm[ _2D(6)]*v[_2D(1)] + tm[ _2D(7)]*v[_2D(2)] + tm[ _2D(8)]*v[_2D(3)] + tm[ _2D(9)]*v[_2D(4)];
      pc[_2D(4)]  = tm[_2D(10)]*v[_2D(0)] + tm[_2D(11)]*v[_2D(1)] + tm[_2D(12)]*v[_2D(2)] + tm[_2D(13)]*v[_2D(3)] + tm[_2D(14)]*v[_2D(4)];
      pc[_2D(7)]  = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(11)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = uc[ _2D(0)]*tm[_2D(10)]+uc[ _2D(1)]*tm[_2D(11)]+uc[ _2D(3)]*tm[_2D(12)]+uc[ _2D(6)]*tm[_2D(13)]+uc[_2D(10)]*tm[_2D(14)];
      v[_2D(1)] = uc[ _2D(1)]*tm[_2D(10)]+uc[ _2D(2)]*tm[_2D(11)]+uc[ _2D(4)]*tm[_2D(12)]+uc[ _2D(7)]*tm[_2D(13)]+uc[_2D(11)]*tm[_2D(14)];
      v[_2D(2)] = uc[ _2D(3)]*tm[_2D(10)]+uc[ _2D(4)]*tm[_2D(11)]+uc[ _2D(5)]*tm[_2D(12)]+uc[ _2D(8)]*tm[_2D(13)]+uc[_2D(12)]*tm[_2D(14)];
      v[_2D(3)] = uc[ _2D(6)]*tm[_2D(10)]+uc[ _2D(7)]*tm[_2D(11)]+uc[ _2D(8)]*tm[_2D(12)]+uc[ _2D(9)]*tm[_2D(13)]+uc[_2D(13)]*tm[_2D(14)];
      v[_2D(4)] = uc[_2D(10)]*tm[_2D(10)]+uc[_2D(11)]*tm[_2D(11)]+uc[_2D(12)]*tm[_2D(12)]+uc[_2D(13)]*tm[_2D(13)]+uc[_2D(14)]*tm[_2D(14)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(5)]  = tm[_2D(10)]*v[_2D(0)] + tm[_2D(11)]*v[_2D(1)] + tm[_2D(12)]*v[_2D(2)] + tm[_2D(13)]*v[_2D(3)] + tm[_2D(14)]*v[_2D(4)];
      pc[_2D(8)]  = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(12)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = uc[ _2D(0)]*tm[_2D(15)]+uc[ _2D(1)]*tm[_2D(16)]+uc[ _2D(3)]*tm[_2D(17)]+uc[ _2D(6)]*tm[_2D(18)]+uc[_2D(10)]*tm[_2D(19)];
      v[_2D(1)] = uc[ _2D(1)]*tm[_2D(15)]+uc[ _2D(2)]*tm[_2D(16)]+uc[ _2D(4)]*tm[_2D(17)]+uc[ _2D(7)]*tm[_2D(18)]+uc[_2D(11)]*tm[_2D(19)];
      v[_2D(2)] = uc[ _2D(3)]*tm[_2D(15)]+uc[ _2D(4)]*tm[_2D(16)]+uc[ _2D(5)]*tm[_2D(17)]+uc[ _2D(8)]*tm[_2D(18)]+uc[_2D(12)]*tm[_2D(19)];
      v[_2D(3)] = uc[ _2D(6)]*tm[_2D(15)]+uc[ _2D(7)]*tm[_2D(16)]+uc[ _2D(8)]*tm[_2D(17)]+uc[ _2D(9)]*tm[_2D(18)]+uc[_2D(13)]*tm[_2D(19)];
      v[_2D(4)] = uc[_2D(10)]*tm[_2D(15)]+uc[_2D(11)]*tm[_2D(16)]+uc[_2D(12)]*tm[_2D(17)]+uc[_2D(13)]*tm[_2D(18)]+uc[_2D(14)]*tm[_2D(19)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(9)]  = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(13)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = uc[ _2D(0)]*tm[_2D(20)]+uc[_2D(1)]*tm[_2D(21)]+uc[_2D(3)]*tm[_2D(22)]+uc[_2D(6)]*tm[_2D(23)]+uc[_2D(10)]*tm[_2D(24)];
      v[_2D(1)] = uc[ _2D(1)]*tm[_2D(20)]+uc[_2D(2)]*tm[_2D(21)]+uc[_2D(4)]*tm[_2D(22)]+uc[_2D(7)]*tm[_2D(23)]+uc[_2D(11)]*tm[_2D(24)];
      v[_2D(2)] = uc[ _2D(3)]*tm[_2D(20)]+uc[_2D(4)]*tm[_2D(21)]+uc[_2D(5)]*tm[_2D(22)]+uc[_2D(8)]*tm[_2D(23)]+uc[_2D(12)]*tm[_2D(24)];
      v[_2D(3)] = uc[ _2D(6)]*tm[_2D(20)]+uc[_2D(7)]*tm[_2D(21)]+uc[_2D(8)]*tm[_2D(22)]+uc[_2D(9)]*tm[_2D(23)]+uc[_2D(13)]*tm[_2D(24)];
      v[_2D(4)] = uc[_2D(10)]*tm[_2D(20)]+uc[_2D(11)]*tm[_2D(21)]+uc[_2D(12)]*tm[_2D(22)]+uc[_2D(13)]*tm[_2D(23)]+uc[_2D(14)]*tm[_2D(24)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(14)]= tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }
  }

  static void similarity_5_5 (
    double_ptr_64_const tm,
    double_ptr_64_const uc,
    double_ptr_64 nm,
    double_ptr_64 pc
  )  {
    // Use nm as a temporary storage
    for (int i=0; i<15; ++i) {
      for (int j=0; j<W; ++j) {
        nm[i*W + j] += uc[i*W + j];
      }
    }

    double _aligned v [5 * W];

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = nm[ _2D(0)]*tm[_2D(0)]+nm[ _2D(1)]*tm[_2D(1)]+nm[ _2D(3)]*tm[_2D(2)]+nm[ _2D(6)]*tm[_2D(3)]+nm[_2D(10)]*tm[_2D(4)];
      v[_2D(1)] = nm[ _2D(1)]*tm[_2D(0)]+nm[ _2D(2)]*tm[_2D(1)]+nm[ _2D(4)]*tm[_2D(2)]+nm[ _2D(7)]*tm[_2D(3)]+nm[_2D(11)]*tm[_2D(4)];
      v[_2D(2)] = nm[ _2D(3)]*tm[_2D(0)]+nm[ _2D(4)]*tm[_2D(1)]+nm[ _2D(5)]*tm[_2D(2)]+nm[ _2D(8)]*tm[_2D(3)]+nm[_2D(12)]*tm[_2D(4)];
      v[_2D(3)] = nm[ _2D(6)]*tm[_2D(0)]+nm[ _2D(7)]*tm[_2D(1)]+nm[ _2D(8)]*tm[_2D(2)]+nm[ _2D(9)]*tm[_2D(3)]+nm[_2D(13)]*tm[_2D(4)];
      v[_2D(4)] = nm[_2D(10)]*tm[_2D(0)]+nm[_2D(11)]*tm[_2D(1)]+nm[_2D(12)]*tm[_2D(2)]+nm[_2D(13)]*tm[_2D(3)]+nm[_2D(14)]*tm[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      pc[ _2D(0)] = tm[ _2D(0)]*v[_2D(0)] + tm[ _2D(1)]*v[_2D(1)] + tm[ _2D(2)]*v[_2D(2)] + tm[ _2D(3)]*v[_2D(3)] + tm[ _2D(4)]*v[_2D(4)];
      pc[ _2D(1)] = tm[ _2D(5)]*v[_2D(0)] + tm[ _2D(6)]*v[_2D(1)] + tm[ _2D(7)]*v[_2D(2)] + tm[ _2D(8)]*v[_2D(3)] + tm[ _2D(9)]*v[_2D(4)];
      pc[ _2D(3)] = tm[_2D(10)]*v[_2D(0)] + tm[_2D(11)]*v[_2D(1)] + tm[_2D(12)]*v[_2D(2)] + tm[_2D(13)]*v[_2D(3)] + tm[_2D(14)]*v[_2D(4)];
      pc[ _2D(6)] = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(10)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] =  nm[ _2D(0)]*tm[_2D(5)]+nm[ _2D(1)]*tm[_2D(6)]+nm[ _2D(3)]*tm[_2D(7)]+nm[ _2D(6)]*tm[_2D(8)]+nm[_2D(10)]*tm[_2D(9)];
      v[_2D(1)] =  nm[ _2D(1)]*tm[_2D(5)]+nm[ _2D(2)]*tm[_2D(6)]+nm[ _2D(4)]*tm[_2D(7)]+nm[ _2D(7)]*tm[_2D(8)]+nm[_2D(11)]*tm[_2D(9)];
      v[_2D(2)] =  nm[ _2D(3)]*tm[_2D(5)]+nm[ _2D(4)]*tm[_2D(6)]+nm[ _2D(5)]*tm[_2D(7)]+nm[ _2D(8)]*tm[_2D(8)]+nm[_2D(12)]*tm[_2D(9)];
      v[_2D(3)] =  nm[ _2D(6)]*tm[_2D(5)]+nm[ _2D(7)]*tm[_2D(6)]+nm[ _2D(8)]*tm[_2D(7)]+nm[ _2D(9)]*tm[_2D(8)]+nm[_2D(13)]*tm[_2D(9)];
      v[_2D(4)] =  nm[_2D(10)]*tm[_2D(5)]+nm[_2D(11)]*tm[_2D(6)]+nm[_2D(12)]*tm[_2D(7)]+nm[_2D(13)]*tm[_2D(8)]+nm[_2D(14)]*tm[_2D(9)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(2)]  = tm[ _2D(5)]*v[_2D(0)] + tm[ _2D(6)]*v[_2D(1)] + tm[ _2D(7)]*v[_2D(2)] + tm[ _2D(8)]*v[_2D(3)] + tm[ _2D(9)]*v[_2D(4)];
      pc[_2D(4)]  = tm[_2D(10)]*v[_2D(0)] + tm[_2D(11)]*v[_2D(1)] + tm[_2D(12)]*v[_2D(2)] + tm[_2D(13)]*v[_2D(3)] + tm[_2D(14)]*v[_2D(4)];
      pc[_2D(7)]  = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(11)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = nm[ _2D(0)]*tm[_2D(10)]+nm[ _2D(1)]*tm[_2D(11)]+nm[ _2D(3)]*tm[_2D(12)]+nm[ _2D(6)]*tm[_2D(13)]+nm[_2D(10)]*tm[_2D(14)];
      v[_2D(1)] = nm[ _2D(1)]*tm[_2D(10)]+nm[ _2D(2)]*tm[_2D(11)]+nm[ _2D(4)]*tm[_2D(12)]+nm[ _2D(7)]*tm[_2D(13)]+nm[_2D(11)]*tm[_2D(14)];
      v[_2D(2)] = nm[ _2D(3)]*tm[_2D(10)]+nm[ _2D(4)]*tm[_2D(11)]+nm[ _2D(5)]*tm[_2D(12)]+nm[ _2D(8)]*tm[_2D(13)]+nm[_2D(12)]*tm[_2D(14)];
      v[_2D(3)] = nm[ _2D(6)]*tm[_2D(10)]+nm[ _2D(7)]*tm[_2D(11)]+nm[ _2D(8)]*tm[_2D(12)]+nm[ _2D(9)]*tm[_2D(13)]+nm[_2D(13)]*tm[_2D(14)];
      v[_2D(4)] = nm[_2D(10)]*tm[_2D(10)]+nm[_2D(11)]*tm[_2D(11)]+nm[_2D(12)]*tm[_2D(12)]+nm[_2D(13)]*tm[_2D(13)]+nm[_2D(14)]*tm[_2D(14)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(5)]  = tm[_2D(10)]*v[_2D(0)] + tm[_2D(11)]*v[_2D(1)] + tm[_2D(12)]*v[_2D(2)] + tm[_2D(13)]*v[_2D(3)] + tm[_2D(14)]*v[_2D(4)];
      pc[_2D(8)]  = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(12)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = nm[ _2D(0)]*tm[_2D(15)]+nm[ _2D(1)]*tm[_2D(16)]+nm[ _2D(3)]*tm[_2D(17)]+nm[ _2D(6)]*tm[_2D(18)]+nm[_2D(10)]*tm[_2D(19)];
      v[_2D(1)] = nm[ _2D(1)]*tm[_2D(15)]+nm[ _2D(2)]*tm[_2D(16)]+nm[ _2D(4)]*tm[_2D(17)]+nm[ _2D(7)]*tm[_2D(18)]+nm[_2D(11)]*tm[_2D(19)];
      v[_2D(2)] = nm[ _2D(3)]*tm[_2D(15)]+nm[ _2D(4)]*tm[_2D(16)]+nm[ _2D(5)]*tm[_2D(17)]+nm[ _2D(8)]*tm[_2D(18)]+nm[_2D(12)]*tm[_2D(19)];
      v[_2D(3)] = nm[ _2D(6)]*tm[_2D(15)]+nm[ _2D(7)]*tm[_2D(16)]+nm[ _2D(8)]*tm[_2D(17)]+nm[ _2D(9)]*tm[_2D(18)]+nm[_2D(13)]*tm[_2D(19)];
      v[_2D(4)] = nm[_2D(10)]*tm[_2D(15)]+nm[_2D(11)]*tm[_2D(16)]+nm[_2D(12)]*tm[_2D(17)]+nm[_2D(13)]*tm[_2D(18)]+nm[_2D(14)]*tm[_2D(19)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(9)]  = tm[_2D(15)]*v[_2D(0)] + tm[_2D(16)]*v[_2D(1)] + tm[_2D(17)]*v[_2D(2)] + tm[_2D(18)]*v[_2D(3)] + tm[_2D(19)]*v[_2D(4)];
      pc[_2D(13)] = tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }

    for (int j=0; j<W; ++j) {
      v[_2D(0)] = nm[ _2D(0)]*tm[_2D(20)]+nm[_2D(1)]*tm[_2D(21)]+nm[_2D(3)]*tm[_2D(22)]+nm[_2D(6)]*tm[_2D(23)]+nm[_2D(10)]*tm[_2D(24)];
      v[_2D(1)] = nm[ _2D(1)]*tm[_2D(20)]+nm[_2D(2)]*tm[_2D(21)]+nm[_2D(4)]*tm[_2D(22)]+nm[_2D(7)]*tm[_2D(23)]+nm[_2D(11)]*tm[_2D(24)];
      v[_2D(2)] = nm[ _2D(3)]*tm[_2D(20)]+nm[_2D(4)]*tm[_2D(21)]+nm[_2D(5)]*tm[_2D(22)]+nm[_2D(8)]*tm[_2D(23)]+nm[_2D(12)]*tm[_2D(24)];
      v[_2D(3)] = nm[ _2D(6)]*tm[_2D(20)]+nm[_2D(7)]*tm[_2D(21)]+nm[_2D(8)]*tm[_2D(22)]+nm[_2D(9)]*tm[_2D(23)]+nm[_2D(13)]*tm[_2D(24)];
      v[_2D(4)] = nm[_2D(10)]*tm[_2D(20)]+nm[_2D(11)]*tm[_2D(21)]+nm[_2D(12)]*tm[_2D(22)]+nm[_2D(13)]*tm[_2D(23)]+nm[_2D(14)]*tm[_2D(24)];
    }

    for (int j=0; j<W; ++j) {
      pc[_2D(14)]= tm[_2D(20)]*v[_2D(0)] + tm[_2D(21)]*v[_2D(1)] + tm[_2D(22)]*v[_2D(2)] + tm[_2D(23)]*v[_2D(3)] + tm[_2D(24)]*v[_2D(4)];
    }
  }

  static void update (
    double_ptr_64 us,
    double_ptr_64 uc,
    double_ptr_64 chi2,
    double_ptr_64_const ps,
    double_ptr_64_const pc,
    std::array<double, 5*W>& Xref,
    std::array<double, 5*W>& H,
    std::array<double, W>& refResidual,
    std::array<double, W>& errorMeas2,
    const std::array<int, W>& mask
  )  {
    for (int j=0; j<W; ++j) {
      for (int i=0; i<5; ++i) {
        us[i*W + j] = ps[i*W + j];
      }

      for (int i=0; i<15; ++i) {
        uc[i*W + j] = pc[i*W + j];
      }
    }

    double _aligned res [W];

    for (int j=0; j<W; ++j){
      res[j] = refResidual[j]
        +  H[_2D(0)] * (Xref[_2D(0)] - us[_2D(0)])
        +  H[_2D(1)] * (Xref[_2D(1)] - us[_2D(1)])
        +  H[_2D(2)] * (Xref[_2D(2)] - us[_2D(2)])
        +  H[_2D(3)] * (Xref[_2D(3)] - us[_2D(3)])
        +  H[_2D(4)] * (Xref[_2D(4)] - us[_2D(4)]);
    }

    double _aligned CHT [5 * W];
    for (int j=0; j<W; ++j){
      CHT[_2D(0)] = uc[_2D( 0)]*H[_2D(0)] + uc[_2D( 1)]*H[_2D(1)] + uc[_2D( 3)]*H[_2D(2)] + uc[_2D( 6)]*H[_2D(3)] + uc[_2D(10)]*H[_2D(4)] ;
      CHT[_2D(1)] = uc[_2D( 1)]*H[_2D(0)] + uc[_2D( 2)]*H[_2D(1)] + uc[_2D( 4)]*H[_2D(2)] + uc[_2D( 7)]*H[_2D(3)] + uc[_2D(11)]*H[_2D(4)] ;
      CHT[_2D(2)] = uc[_2D( 3)]*H[_2D(0)] + uc[_2D( 4)]*H[_2D(1)] + uc[_2D( 5)]*H[_2D(2)] + uc[_2D( 8)]*H[_2D(3)] + uc[_2D(12)]*H[_2D(4)] ;
      CHT[_2D(3)] = uc[_2D( 6)]*H[_2D(0)] + uc[_2D( 7)]*H[_2D(1)] + uc[_2D( 8)]*H[_2D(2)] + uc[_2D( 9)]*H[_2D(3)] + uc[_2D(13)]*H[_2D(4)] ;
      CHT[_2D(4)] = uc[_2D(10)]*H[_2D(0)] + uc[_2D(11)]*H[_2D(1)] + uc[_2D(12)]*H[_2D(2)] + uc[_2D(13)]*H[_2D(3)] + uc[_2D(14)]*H[_2D(4)] ;
    }

    double _aligned errorResInv2 [W];
    for (int j=0; j<W; ++j){
      errorResInv2[j] =
        (mask[j]==0 ? 0 :
        1. / (errorMeas2[j]
        + H[_2D(0)]*CHT[_2D(0)]
        + H[_2D(1)]*CHT[_2D(1)]
        + H[_2D(2)]*CHT[_2D(2)]
        + H[_2D(3)]*CHT[_2D(3)]
        + H[_2D(4)]*CHT[_2D(4)]));
    }

    // update the state vector and cov matrix
    double _aligned w [W];
    for (int j=0; j<W; ++j){
      w[j] = res[j] * errorResInv2[j];
    }

    for(int j=0; j<W; ++j){
      us[_2D(0)] += CHT[_2D(0)] * w[j];
      us[_2D(1)] += CHT[_2D(1)] * w[j];
      us[_2D(2)] += CHT[_2D(2)] * w[j];
      us[_2D(3)] += CHT[_2D(3)] * w[j];
      us[_2D(4)] += CHT[_2D(4)] * w[j];
    }

    for (int j=0; j<W; ++j){
      uc[_2D( 0)] -= errorResInv2[j] * CHT[_2D(0)] * CHT[_2D(0)];
      uc[_2D( 1)] -= errorResInv2[j] * CHT[_2D(1)] * CHT[_2D(0)];
      uc[_2D( 3)] -= errorResInv2[j] * CHT[_2D(2)] * CHT[_2D(0)];
      uc[_2D( 6)] -= errorResInv2[j] * CHT[_2D(3)] * CHT[_2D(0)];
      uc[_2D(10)] -= errorResInv2[j] * CHT[_2D(4)] * CHT[_2D(0)];

      uc[_2D( 2)] -= errorResInv2[j] * CHT[_2D(1)] * CHT[_2D(1)];
      uc[_2D( 4)] -= errorResInv2[j] * CHT[_2D(2)] * CHT[_2D(1)];
      uc[_2D( 7)] -= errorResInv2[j] * CHT[_2D(3)] * CHT[_2D(1)];
      uc[_2D(11)] -= errorResInv2[j] * CHT[_2D(4)] * CHT[_2D(1)];

      uc[_2D( 5)] -= errorResInv2[j] * CHT[_2D(2)] * CHT[_2D(2)];
      uc[_2D( 8)] -= errorResInv2[j] * CHT[_2D(3)] * CHT[_2D(2)];
      uc[_2D(12)] -= errorResInv2[j] * CHT[_2D(4)] * CHT[_2D(2)];

      uc[_2D( 9)] -= errorResInv2[j] * CHT[_2D(3)] * CHT[_2D(3)];
      uc[_2D(13)] -= errorResInv2[j] * CHT[_2D(4)] * CHT[_2D(3)];

      uc[_2D(14)] -= errorResInv2[j] * CHT[_2D(4)] * CHT[_2D(4)];
    }

    for (int j=0; j<W; ++j){
      chi2[j] = res[j] * res[j] * errorResInv2[j];
    }
  }
};

template<class T>
void math_similarity_5_5_nonvec (
  const TrackMatrix& tm,
  const T& uc,
  nTrackSymMatrix& pc
) {
  auto _0 = uc[ 0]*tm[0]+uc[ 1]*tm[1]+uc[ 3]*tm[2]+uc[ 6]*tm[3]+uc[10]*tm[4];
  auto _1 = uc[ 1]*tm[0]+uc[ 2]*tm[1]+uc[ 4]*tm[2]+uc[ 7]*tm[3]+uc[11]*tm[4];
  auto _2 = uc[ 3]*tm[0]+uc[ 4]*tm[1]+uc[ 5]*tm[2]+uc[ 8]*tm[3]+uc[12]*tm[4];
  auto _3 = uc[ 6]*tm[0]+uc[ 7]*tm[1]+uc[ 8]*tm[2]+uc[ 9]*tm[3]+uc[13]*tm[4];
  auto _4 = uc[10]*tm[0]+uc[11]*tm[1]+uc[12]*tm[2]+uc[13]*tm[3]+uc[14]*tm[4];
  pc[ 0] = tm[ 0]*_0 + tm[ 1]*_1 + tm[ 2]*_2 + tm[ 3]*_3 + tm[ 4]*_4;
  pc[ 1] = tm[ 5]*_0 + tm[ 6]*_1 + tm[ 7]*_2 + tm[ 8]*_3 + tm[ 9]*_4;
  pc[ 3] = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
  pc[ 6] = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[10] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4; 
  _0 =  uc[ 0]*tm[5]+uc[ 1]*tm[6]+uc[ 3]*tm[7]+uc[ 6]*tm[8]+uc[10]*tm[9];
  _1 =  uc[ 1]*tm[5]+uc[ 2]*tm[6]+uc[ 4]*tm[7]+uc[ 7]*tm[8]+uc[11]*tm[9];
  _2 =  uc[ 3]*tm[5]+uc[ 4]*tm[6]+uc[ 5]*tm[7]+uc[ 8]*tm[8]+uc[12]*tm[9];
  _3 =  uc[ 6]*tm[5]+uc[ 7]*tm[6]+uc[ 8]*tm[7]+uc[ 9]*tm[8]+uc[13]*tm[9];
  _4 =  uc[10]*tm[5]+uc[11]*tm[6]+uc[12]*tm[7]+uc[13]*tm[8]+uc[14]*tm[9];
  pc[2]  = tm[ 5]*_0 + tm[ 6]*_1 + tm[ 7]*_2 + tm[ 8]*_3 + tm[ 9]*_4;
  pc[4]  = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
  pc[7]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[11] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
  _0 = uc[ 0]*tm[10]+uc[ 1]*tm[11]+uc[ 3]*tm[12]+uc[ 6]*tm[13]+uc[10]*tm[14];
  _1 = uc[ 1]*tm[10]+uc[ 2]*tm[11]+uc[ 4]*tm[12]+uc[ 7]*tm[13]+uc[11]*tm[14];
  _2 = uc[ 3]*tm[10]+uc[ 4]*tm[11]+uc[ 5]*tm[12]+uc[ 8]*tm[13]+uc[12]*tm[14];
  _3 = uc[ 6]*tm[10]+uc[ 7]*tm[11]+uc[ 8]*tm[12]+uc[ 9]*tm[13]+uc[13]*tm[14];
  _4 = uc[10]*tm[10]+uc[11]*tm[11]+uc[12]*tm[12]+uc[13]*tm[13]+uc[14]*tm[14];
  pc[5]  = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
  pc[8]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[12] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
  _0 = uc[ 0]*tm[15]+uc[ 1]*tm[16]+uc[ 3]*tm[17]+uc[ 6]*tm[18]+uc[10]*tm[19];
  _1 = uc[ 1]*tm[15]+uc[ 2]*tm[16]+uc[ 4]*tm[17]+uc[ 7]*tm[18]+uc[11]*tm[19];
  _2 = uc[ 3]*tm[15]+uc[ 4]*tm[16]+uc[ 5]*tm[17]+uc[ 8]*tm[18]+uc[12]*tm[19];
  _3 = uc[ 6]*tm[15]+uc[ 7]*tm[16]+uc[ 8]*tm[17]+uc[ 9]*tm[18]+uc[13]*tm[19];
  _4 = uc[10]*tm[15]+uc[11]*tm[16]+uc[12]*tm[17]+uc[13]*tm[18]+uc[14]*tm[19];
  pc[9]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
  pc[13] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
  _0 = uc[ 0]*tm[20]+uc[1]*tm[21]+uc[3]*tm[22]+uc[6]*tm[23]+uc[10]*tm[24];
  _1 = uc[ 1]*tm[20]+uc[2]*tm[21]+uc[4]*tm[22]+uc[7]*tm[23]+uc[11]*tm[24];
  _2 = uc[ 3]*tm[20]+uc[4]*tm[21]+uc[5]*tm[22]+uc[8]*tm[23]+uc[12]*tm[24];
  _3 = uc[ 6]*tm[20]+uc[7]*tm[21]+uc[8]*tm[22]+uc[9]*tm[23]+uc[13]*tm[24];
  _4 = uc[10]*tm[20]+uc[11]*tm[21]+uc[12]*tm[22]+uc[13]*tm[23]+uc[14]*tm[24];
  pc[14]= tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
}

void math_update_nonvec (
  nTrackVector& X,
  nTrackSymMatrix& C,
  double& chi2,
  const double* Xref,
  const double* H,
  double refResidual,
  double errorMeas2
);
