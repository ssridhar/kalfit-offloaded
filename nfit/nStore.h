#pragma once

#include <vector>
#include "assert.h"
#include "../oldfit/MatrixTypes.h"

#define VECTOR_WIDTH 8

struct nTrackVector {
  double* basePointer = 0x0;
  int stride = VECTOR_WIDTH;

  nTrackVector () = default;
  nTrackVector (double* basePointer, int stride) :
    basePointer(basePointer), stride(stride) {}
  
  /**
   * @brief      Copies v into its state
   *             Assumes basePointer is well defined
   */
  void copy (const nTrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }

  void copy (const TrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }

  void operator= (const TrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }

  void operator+= (const TrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) += v[i];
    }
  }

  // Request item i
  double& operator[] (const unsigned int i) { return basePointer[i * stride]; }
  
  double operator[] (const unsigned int i) const { return basePointer[i * stride]; }

  void setBasePointer (const nTrackVector& v) {
    basePointer = v.basePointer;
  }
  
  void setBasePointer (double* basePointer) {
    basePointer = basePointer;
  }
};

struct nTrackSymMatrix {
  double* basePointer = 0x0;
  int stride = VECTOR_WIDTH;

  nTrackSymMatrix () = default;
  nTrackSymMatrix (double* basePointer, int stride) :
    basePointer(basePointer), stride(stride) {}

  /**
   * @brief      Copies v into its state
   *             Assumes basePointer is well defined
   */
  void copy (const nTrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }

  void copy (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }

  void operator= (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }

  void operator+= (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) += v[i];
    }
  }

  // Request item i
  double& operator[] (const unsigned int i) { return basePointer[i * stride]; }
  
  double operator[] (const unsigned int i) const { return basePointer[i * stride]; }

  double& operator()(const int row, const int col) {
    return row>col ?
      basePointer[(row*(row+1)/2 + col) * stride] :
      basePointer[(col*(col+1)/2 + row) * stride];
  }

  double operator()(const int row, const int col) const {
    return row>col ?
      basePointer[(row*(row+1)/2 + col) * stride] :
      basePointer[(col*(col+1)/2 + row) * stride];
  }

  void setBasePointer (const nTrackSymMatrix& m) {
    basePointer = m.basePointer;
  }

  void setBasePointer (double* basePointer) {
    basePointer = basePointer;
  }
};

struct MemManager {
  std::vector<double> backend;

  int stride = VECTOR_WIDTH;
  int width = 41;
  
  int currentElement = 0;
  int vectorNumber = 0;

  MemManager (
    const int& width,
    const int& stride,
    const int& numberOfNodes
  ) : width(width), stride(stride) {
    backend.resize(numberOfNodes * width);
  }

  double* getNewVector () {
    if (currentElement != 0) {
      // We were requested a new vector,
      // but we are not on the initial element
      // 
      // Check if we still have space for another vector,
      // otherwise TODO (for the moment resize)
      ++vectorNumber;
      currentElement = 0;

      // assert(backend.size() > (vectorNumber + 1) * stride * width);
    }
    double* d = backend.data() + vectorNumber * stride * width;
    ++vectorNumber;
    return d;
  }

  double* getNextElement () {
    double* d = backend.data() + vectorNumber * stride * width + currentElement;
    ++currentElement;
    if (currentElement == stride) {
      ++vectorNumber;
      currentElement = 0;

      // assert(backend.size() > (vectorNumber + 1) * stride * width);
    }
    return d;
  }

  double* getLastVector () {
    // assert(vectorNumber >= 1);
    
    return backend.data() + (vectorNumber - 1) * stride * width;
  }
};
