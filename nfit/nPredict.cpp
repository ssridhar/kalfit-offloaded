#include "nPredict.h"

// Forward predict

template<>
void npredict<Fit::Forward, false> (
  nFitNode& node,
  const nFitNode& prevnode
) {
  node.getState<Fit::Forward, Fit::Predicted, Fit::StateVector>().copy(node.m_refVector.m_parameters);
  node.getState<Fit::Forward, Fit::Predicted, Fit::Covariance>().setBasePointer(prevnode.getState<Fit::Forward, Fit::Updated, Fit::Covariance>());
}

template<>
void npredict<Fit::Forward, true> (
  nFitNode& node,
  const nFitNode& prevnode
) {
  // update state
  node.getState<Fit::Forward, Fit::Predicted, Fit::StateVector>().copy(node.m_transportVector);
  for (int i=0; i<5; ++i) {
    for (int j=0; j<5; ++j) {
      node.getState<Fit::Forward, Fit::Predicted, Fit::StateVector>()[i] += 
        node.getFit<Fit::Forward>().m_transportMatrix[5*i + j] *
        prevnode.getState<Fit::Forward, Fit::Updated, Fit::StateVector>()[j];
    }
  }

  // update covariance
  ntransportCovariance(
    node.getFit<Fit::Forward>().m_transportMatrix,
    prevnode.getState<Fit::Forward, Fit::Updated, Fit::Covariance>(),
    node.getState<Fit::Forward, Fit::Predicted, Fit::Covariance>());
  node.getState<Fit::Forward, Fit::Predicted, Fit::Covariance>() += node.m_noiseMatrix;
}

// Backward predict

template<>
void npredict<Fit::Backward, false> (
  nFitNode& node,
  const nFitNode& prevnode
) {
  // predictedState.m_stateVector = node.getState<Fit::Forward, Fit::Updated, Fit::StateVector>(); // Why?
  // predictedState.m_covariance  = prevCovariance;
  node.getState<Fit::Backward, Fit::Predicted, Fit::StateVector>().setBasePointer(node.getState<Fit::Forward, Fit::Updated, Fit::StateVector>());
  node.getState<Fit::Backward, Fit::Predicted, Fit::Covariance>().setBasePointer(prevnode.getState<Fit::Backward, Fit::Updated, Fit::Covariance>());
}

template<>
void npredict<Fit::Backward, true> (
  nFitNode& node,
  const nFitNode& prevnode
) {
  // predictedState.m_stateVector = prevnode.m_backwardFit.m_transportMatrix * (prevStateVector - prevnode.m_transportVector);
  TrackVector temp_sub;
  for (int i=0; i<5; ++i) {
    temp_sub[i] = prevnode.getState<Fit::Backward, Fit::Updated, Fit::StateVector>()[i] - prevnode.m_transportVector[i];
  };

  for (int i=0; i<5; ++i) {
    node.getState<Fit::Backward, Fit::Predicted, Fit::StateVector>()[i] = prevnode.getFit<Fit::Backward>().m_transportMatrix[5*i] * temp_sub[0];
    for (int j=1; j<5; ++j) {
      node.getState<Fit::Backward, Fit::Predicted, Fit::StateVector>()[i] += prevnode.getFit<Fit::Backward>().m_transportMatrix[5*i + j] * temp_sub[j];
    }
  }

  // TrackSymMatrix tempcov = prevCovariance + prevnode.m_noiseMatrix; // Why does the order change?
  // transportCovariance(prevnode.m_backwardFit.m_transportMatrix, tempcov, predictedState.m_covariance);
  TrackSymMatrix temp_cov;
  for (int i=0; i<15; ++i) {
    temp_cov[i] = prevnode.getState<Fit::Backward, Fit::Updated, Fit::Covariance>()[i] + prevnode.m_noiseMatrix[i];
  }

  ntransportCovariance (
    prevnode.getFit<Fit::Backward>().m_transportMatrix,
    temp_cov,
    node.getState<Fit::Backward, Fit::Predicted, Fit::Covariance>()
  );
}
