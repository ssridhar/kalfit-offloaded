/**
 *      Timer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#include "Timer.h"

Timer::Timer() : _append(0), timeDiff(0) {}

void Timer::start(){
    clock_gettime(CLOCK_REALTIME, &tstart);
}

void Timer::stop(){
    clock_gettime(CLOCK_REALTIME, &tend);
    timeDiff += (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
}

void Timer::flush(){
    timeDiff = 0;
}

double Timer::getElapsedTime(){
    clock_gettime(CLOCK_REALTIME, &tend);
    timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
    return ((double) timeDiff) / 1000000000.0;
}

long long int Timer::getElapsedTimeLong(){
    clock_gettime(CLOCK_REALTIME, &tend);
    timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
    return timeDiff;
}

double Timer::get() const {
    return ((double) timeDiff) / 1000000000.0f;
}

long long int Timer::getLong() const {
    return timeDiff;
}
