#include "Tools.h"

// #define M_LOG2E 1.44269504088896340736 //log2(e)
inline long double log2(const long long x) {
    return log(x) * M_LOG2E;
}

void print(const Matrix1x5& m, const std::string& name) {
  std::cout << name << ": ";
  for (int i=0; i<5; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const Matrix5x5& m, const std::string& name) {
  std::cout << name << ": ";
  for (int i=0; i<25; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const SymMatrix5x5& m, const std::string& name) {
  std::cout << name << ": ";
  for (int i=0; i<15; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const ChiSquare& c, const std::string& name) {
  std::cout << name << ":" << std::endl
    << "  m_chi2: " << c.m_chi2 << std::endl
    << "  m_nDoF: " << c.m_nDoF << std::endl;
}

void print(const XYZVector& x, const std::string& name) {
  std::cout << name << ":" << std::endl
    << "  x, y, z: " << x.fX << ", " << x.fY << ", " << x.fZ << std::endl;
}

void print(const State& s, const std::string& name) {
  std::cout << name << ":" << std::endl;
  std::cout << "  m_flags: " << s.m_flags << std::endl;
  print(s.m_stateVector, "  m_stateVector");
  print(s.m_covariance, "  m_covariance");
  std::cout << "  m_z: " << s.m_z << std::endl;
}

void print(const StateVector& s, const std::string& name) {
  std::cout << name << ":" << std::endl;
  print(s.m_parameters, "  m_parameters");
  std::cout << "  m_z: " << s.m_z << std::endl;
}

void print(const Node& n, const std::string& name) {
  std::cout << name << ":" << std::endl;
  std::cout << " m_type: " << n.m_type << std::endl;
  print(n.m_state, " m_state");
  print(n.m_refVector, " m_refVector");
  std::cout << " m_refIsSet: " << n.m_refIsSet << std::endl
    << " m_measurement: " << n.m_measurement << std::endl
    << " m_residual: " << n.m_residual << std::endl
    << " m_errResidual: " << n.m_errResidual << std::endl
    << " m_errMeasure: " << n.m_errMeasure << std::endl;
  print(n.m_projectionMatrix, " m_projectionMatrix");
}

void print(const FitNode& f, const std::string& name) {
  std::cout << name << ": " << std::endl;
  print(*(dynamic_cast<Node*>(& const_cast<FitNode&>(f))), " Node");
  print(f.m_transportMatrix, " m_transportMatrix");
  print(f.m_invertTransportMatrix, " m_invertTransportMatrix");
  print(f.m_transportVector, " m_transportVector");
  print(f.m_noiseMatrix, " m_noiseMatrix");
  std::cout << " m_deltaEnergy: " << f.m_deltaEnergy << std::endl
    << " m_refResidual: " << f.m_refResidual << std::endl
    << " m_filterStatus: " << f.m_filterStatus[0] << ", " << f.m_filterStatus[1] << std::endl
    << " m_hasInfoUpstream: " << f.m_hasInfoUpstream[0] << ", " << f.m_hasInfoUpstream[1] << std::endl;
  print(f.m_predictedState[0], " m_predictedState[0]");
  print(f.m_predictedState[1], " m_predictedState[1]");
  print(f.m_filteredState[0], " m_filteredState[0]");
  print(f.m_filteredState[1], " m_filteredState[1]");
  print(f.m_classicalSmoothedState, " m_classicalSmoothedState");
  print(f.m_deltaChi2[0], " m_deltaChi2[0]");
  print(f.m_deltaChi2[1], " m_deltaChi2[1]");
  print(f.m_totalChi2[0], " m_totalChi2[0]");
  print(f.m_totalChi2[1], " m_totalChi2[1]");
  print(f.m_smootherGainMatrix, " m_smootherGainMatrix");
  print(f.m_pocaVector, " m_pocaVector");
  std::cout << " m_doca: " << f.m_doca << std::endl
    << " m_chi2Double: " << f.m_chi2Double << std::endl;
}

bool compare(const double& d1, const double& d2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&d1);
  const long long* i2 = reinterpret_cast<const long long*>(&d2);

  long long epsilon = 0;
  epsilon = std::max(epsilon, std::abs(*i1 - *i2));
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const Matrix1x5& m1, const Matrix1x5& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (int i=0; i<5; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));

    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const Matrix5x5& m1, const Matrix5x5& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (int i=0; i<25; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));
    
    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: " << "bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const SymMatrix5x5& m1, const SymMatrix5x5& m2, const double max_epsilon, const bool doPrint) {
  const long long* i1;
  const long long* i2;
  double d1, d2;

  long long epsilon = 0;
  for (int i=0; i<15; ++i) {
    i1 = reinterpret_cast<const long long*>(&m1.fArray[i]);
    i2 = reinterpret_cast<const long long*>(&m2.fArray[i]);

    long long prevEpsilon = epsilon;
    epsilon = std::max(epsilon, std::abs(*i1 - *i2));

    if (prevEpsilon != epsilon) {
      d1 = m1.fArray[i];
      d2 = m2.fArray[i];
    }
  }
  
  if (epsilon > max_epsilon) {
    if (doPrint) std::cout << "max epsilon: " << "bit " << floor(log2(epsilon))
      << " (" << d1 << " and " << d2 << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare(const ChiSquare& c1, const ChiSquare& c2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&c1.m_chi2);
  const long long* i2 = reinterpret_cast<const long long*>(&c2.m_chi2);

  long long epsilon = 0;
  bool equal = true;

  epsilon = std::max(epsilon, std::abs(*i1 - *i2));
  if (epsilon > max_epsilon) {
    equal = false;
    if (doPrint) std::cout << "Chi2 mismatch " << "(" << "bit " << floor(log2(epsilon)) << ") "
      << "(" << c1.m_chi2 << ", " << c2.m_chi2 << ")" << std::endl;
  }

  if (c1.m_nDoF != c2.m_nDoF) {
    equal = false;
    if (doPrint) std::cout << "nDoF mismatch " << "(" << c1.m_nDoF << ", " << c2.m_nDoF << ")" << std::endl;
  }

  return equal;
}

bool compare(const State& s1, const State& s2, const double max_epsilon, const bool doPrint) {
  const long long* i1 = reinterpret_cast<const long long*>(&s1.m_z);
  const long long* i2 = reinterpret_cast<const long long*>(&s2.m_z);

  bool equal = true;

  // if (s1.m_flags != s2.m_flags) {
  //   if (doPrint) std::cout << "Flags mismatch" << std::endl;
  //   equal = false;
  // }

  if (!compare(s1.m_stateVector, s2.m_stateVector, max_epsilon, doPrint)) {
    if (doPrint) std::cout << "StateVector mismatch" << std::endl;
    equal = false;
  }

  if (!compare(s1.m_covariance, s2.m_covariance, max_epsilon, doPrint)) {
    if (doPrint) std::cout << "Covariance mismatch" << std::endl;
    equal = false;
  }

  // long long epsilon = std::abs(*i1 - *i2);
  // if (epsilon > max_epsilon) {
  //   if (doPrint) std::cout << "State z mismatch" << std::endl;
  //   equal = false;
  // }

  return equal;
}

bool compare(
  const FitNode& f1,
  const FitNode& f2,
  const double max_epsilon,
  const bool doPrint,
  const int compareChi2
) {
  // For now, let's compare the matrices
  bool equal = true;

  auto test = [&equal, &doPrint] (const bool& result, const std::string& printout) {
    if (!result) {
      equal = false;
      if (doPrint) {
        std::cout << printout << std::endl;
      }
    }
  };
  
  // Compare states
  test(compare(f1.m_predictedState[0], f2.m_predictedState[0], max_epsilon, doPrint), "Predicted state [0] mismatch");
  test(compare(f1.m_filteredState[0], f2.m_filteredState[0], max_epsilon, doPrint), "Filtered state [0] mismatch");
  test(compare(f1.m_predictedState[1], f2.m_predictedState[1], max_epsilon, doPrint), "Predicted state [1] mismatch");
  test(compare(f1.m_filteredState[1], f2.m_filteredState[1], max_epsilon, doPrint), "Filtered state [1] mismatch");

  // test(compare(f1.m_state, f2.m_state, max_epsilon, doPrint), "State mismatch");
  test(compare(f1.m_transportMatrix, f2.m_transportMatrix, max_epsilon, doPrint), "Transport matrix mismatch");
  test(compare(f1.m_invertTransportMatrix, f2.m_invertTransportMatrix, max_epsilon, doPrint), "Inverse transport matrix mismatch");
  test(compare(f1.m_transportVector, f2.m_transportVector, max_epsilon, doPrint), "Transport vector mismatch");
  
  if (compareChi2 == 0)
    test(compare(f1.m_totalChi2[0], f2.m_totalChi2[0], max_epsilon, doPrint), "totalChi2 [0] mismatch");
  else if (compareChi2 == 1)
    test(compare(f1.m_totalChi2[1], f2.m_totalChi2[1], max_epsilon, doPrint), "totalChi2 [1] mismatch");

  // test(compare(f1.m_errMeasure, f2.m_errMeasure, max_epsilon, doPrint), "m_errMeasure mismatch");
  // test(compare(f1.m_errResidual, f2.m_errResidual, max_epsilon, doPrint), "m_errResidual mismatch");
  // test(compare(f1.m_residual, f2.m_residual, max_epsilon, doPrint), "m_residual mismatch");

  return equal;
}

void setPrevNode(FitNode& node, FitNode& prevnode, int direction) {
  if (direction==Forward) node.m_prevNode = &prevnode;
  else node.m_nextNode = &prevnode;
}

std::vector<Instance> translateFileIntoEvent (const std::vector<uint8_t>& fileContents) {
  std::vector<Instance> t;
  uint8_t* p = const_cast<uint8_t*>(fileContents.data());

  while (p < fileContents.data() + fileContents.size()) {
    Instance instance;

    // instance.doForwardFit = (bool) *p; p += sizeof(uint8_t);
    // instance.doBackwardFit = (bool) *p; p += sizeof(uint8_t);
    p += sizeof(uint8_t);
    instance.doBismooth = (bool) *p; p += sizeof(uint8_t);
    int numberOfTracks = *((int*) p); p += sizeof(uint32_t);

    instance.tracks.resize(numberOfTracks);
    instance.expectedResult.resize(numberOfTracks);

    for (int i=0; i<numberOfTracks; ++i) {
      int memblockSize = *((int*) p); p += sizeof(uint32_t);
      
      instance.tracks[i].resize(memblockSize);
      instance.expectedResult[i].resize(memblockSize);

      std::copy(p, p + memblockSize * sizeof(FitNode), ((uint8_t*) instance.tracks[i].data())); p += memblockSize * sizeof(FitNode);
      std::copy(p, p + memblockSize * sizeof(FitNode), ((uint8_t*) instance.expectedResult[i].data())); p += memblockSize * sizeof(FitNode);
    }
    
    // Revise all this when new datatype is there
    instance.doForwardFit = false;
    instance.doBackwardFit = false;

    if (instance.tracks.size() > 0 && instance.tracks[0].size() > 0) {
      auto doFit = [&] (const int& direction) {
        for (const auto& t : instance.tracks) {
          for (const auto& n : t) {
            if (n.requirePredictedState(direction)) {
              return true;
            }
          }
        }
        return false;
      };

      instance.doForwardFit = doFit(Forward);
      instance.doBackwardFit = instance.doForwardFit || doFit(Backward);

      auto isOutlier = [&] {
        for (const auto& n : instance.tracks[0]) {
          if (!n.requirePredictedState(Forward) || !n.requirePredictedState(Forward)) {
            return true;
          }
        }
        return false;
      };
      instance.isOutlier = (instance.doForwardFit || instance.doBackwardFit) && isOutlier();

      if (instance.isOutlier) {
        // Search for the outliers
        instance.outliers = std::vector<int>(instance.tracks.size(), -1);
        for (int i=0; i<instance.tracks.size(); ++i) {
          bool found = false;
          const std::vector<FitNode>& track = instance.tracks[i];

          for (int j=0; j<track.size(); ++j) {
            const FitNode& node = track[j];
            if (node.requirePredictedState(Forward) == false and node.requirePredictedState(Backward) == false) {
              instance.outliers[i] = j;
              found = true;
              break;
            }
          }

          if (found) continue;
        }
      }
    }

    // Generate new tracks
    for (int i=0; i<instance.tracks.size(); ++i) {
    // for (const auto& track : instance.tracks) {
      instance.ntracks.push_back(nTrack(instance.tracks[i], i));
    }

    // Add info on outlier
    if (instance.isOutlier) {
      for (int i=0; i<instance.ntracks.size(); ++i) {
        instance.ntracks[i].outlier = instance.outliers[i];
      }
    }

    t.push_back(instance);
  }

  assert(p == fileContents.data() + fileContents.size());
  return t;
}

void printInfo(const std::vector<Instance>& t, std::vector<GroupInfo>& groups) {
  std::cout << t.size() << " track groups successfully read!" << std::endl;

  groups.resize(t.size());

  int noForwardFit = 0;
  int noBackwardFit = 0;
  int noBismooth = 0;
  int noForwardExecute = 0;
  int noBackwardExecute = 0;
  int noBismoothExecute = 0;
  for (int i=0; i<t.size(); ++i) {
    noForwardFit += t[i].doBackwardFit;
    noBackwardFit += t[i].doBackwardFit;
    noBismooth += t[i].doBismooth;
    for (int j=0; j<t[i].tracks.size(); ++j) {
      noForwardExecute += t[i].doForwardFit * t[i].tracks[j].size();
      noBackwardExecute += t[i].doBackwardFit * t[i].tracks[j].size();
      noBismoothExecute += t[i].doBismooth * t[i].tracks[j].size();
      groups[i].numberOfNodes += t[i].tracks[j].size();
    }
  }

  int totalNumberOfNodes = [&] {int acc = 0; for (auto group : groups) acc += group.numberOfNodes; return acc; } ();
  std::cout << " total number of nodes: " << totalNumberOfNodes << std::endl
    << " fit requested on " << noForwardFit << " track groups (" << noForwardExecute << " nodes)" << std::endl
    << " backward fit requested on " << noBackwardFit << " track groups (" << noBackwardExecute << " nodes)" << std::endl
    << " smoother fit requested on " << noBismooth << " track groups (" << noBismoothExecute << " nodes)" << std::endl;

  std::cout << " #tracks (doForwardFit, doBackwardFit, doSmoother): ";
  for (int i=0; i<t.size(); ++i)
    std::cout << t[i].tracks.size() << " (" << t[i].doForwardFit << ", " << t[i].doBackwardFit << ", " << t[i].doBismooth << "), ";
  std::cout << std::endl << std::endl;
}

void postPrint(
  const bool allEqual,
  const bool printInfoAboutGroups,
  const std::vector<Instance>& t,
  const std::vector<GroupInfo>& groups
) {
  std::cout << std::endl << std::endl;

  if (allEqual) {
    std::cout << "All results are equal!" << std::endl << std::endl;
  }
  
  if (printInfoAboutGroups) {
    int totalFiltered=0, totalPredicted=0, totalBismoothed=0;
    for (int i=0; i<t.size(); ++i) {
      std::cout << "Group #" << i << " (" << t[i].doBackwardFit << ", " << t[i].doBismooth << "), "
        << groups[i].numberOfNodes << " nodes: " << groups[i].filtered << " filtered, " << groups[i].predicted << " predicted"
        << (t[i].doBismooth ? ", " + std::to_string(groups[i].bismoothed) + " smoothed" : "") << std::endl;

      totalFiltered += groups[i].filtered;
      totalPredicted += groups[i].predicted;
      totalBismoothed += groups[i].bismoothed;
    }
    std::cout << "Total statistics: " << totalFiltered << " filtered, " << totalPredicted << " predicted, "
      << totalBismoothed << " smoothed" << std::endl << std::endl;
  }
}

std::string shellexec (const char* cmd) {
  char buffer[128];
  std::string result = "";
  std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
  if (!pipe) throw std::runtime_error("popen() failed!");
  while (!feof(pipe.get())) {
    if (fgets(buffer, 128, pipe.get()) != NULL)
      result += buffer;
  }
  return result;
}
