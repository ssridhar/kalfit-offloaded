#!/bin/bash

if [ $# -lt 2 ] ; then
  echo "Usage: calculate executable output_folder"
  exit 0
fi

# Executable should be passed by arguments
CALL=$1
FOLDER=$2
PATH=$PWD

# Get core count for one single CPU
CPU_NAME=`cat /proc/cpuinfo | grep "model name" | tail -1 | sed -e 's/model name\t: //'`
PACKAGES=$((`cat /proc/cpuinfo | awk '/^physical id/{print $4}' | tail -1` + 1))
CORE_COUNT=$((`numactl --show | grep physcpubind | awk '{print $NF}'` + 1))
NUMA_NODES=$((`numactl --show | grep ^cpubind | awk '{print $NF}'` + 1))

mkdir -p $2
cd $2
echo '' > average_times.txt

printf "Machine description: \n"\
" $CPU_NAME \n"\
" $PACKAGES packages, $NUMA_NODES NUMA nodes, $CORE_COUNT cores \n" | tee -a average_times.txt

# Start CPU utilization script
/bin/bash $PATH/measure_cpu_utilization.sh measure_cpu_utilization cpu_utilization 1 &
CPUUTIL_PID=$!

# Run as many instances as needed to occupy all cores
printf "\nStarting $CORE_COUNT processes\n"\
" $CALL\n " | tee -a average_times.txt

for i in `seq 1 $CORE_COUNT`
do
  printf "." | tee -a average_times.txt
  mkdir -p $i
  cd $i
  $CALL > output.txt 2>&1 &
  cd ..
done
printf "\n" | tee -a average_times.txt

cd $PATH
wait

# Parse results when complete
kill $CPUUTIL_PID
cd $2

# Empty files
echo '' > forward_times.txt
echo '' > backward_times.txt
echo '' > bismooth_times.txt

# Calculate average times
for i in `seq 1 $CORE_COUNT`
do
  cat $i/output.txt | grep "Forward fit mean:" | awk '{print $6}' >> forward_times.txt
  cat $i/output.txt | grep "Backward fit mean:" | awk '{print $6}' >> backward_times.txt
  cat $i/output.txt | grep "Bismooth fit mean:" | awk '{print $6}' >> bismooth_times.txt
done
TIME_AVG_FORWARD=`cat forward_times.txt | awk '{sum += $1} END {print sum / NR}'`
TIME_AVG_BACKWARD=`cat backward_times.txt | awk '{sum += $1} END {print sum / NR}'`
TIME_AVG_BISMOOTH=`cat bismooth_times.txt | awk '{sum += $1} END {print sum / NR}'`

printf "\nTime averages:\n"\
"Forward fit: $TIME_AVG_FORWARD s\n"\
"Backward fit: $TIME_AVG_BACKWARD s\n"\
"Bismooth fit: $TIME_AVG_BISMOOTH s\n" | tee -a average_times.txt

# Attempt to get also throughput
FILTERED_EXECUTED=`cat 1/output.txt | grep "Total statistics:" | awk '{print $3}'`
PREDICTED_EXECUTED=`cat 1/output.txt | grep "Total statistics:" | awk '{print $5}'`
BISMOOTHED_EXECUTED=`cat 1/output.txt | grep "Total statistics:" | awk '{print $7}'`

echo $FILTERED_EXECUTED $PREDICTED_EXECUTED $CORE_COUNT $TIME_AVG_FORWARD $TIME_AVG_BACKWARD \
  | awk '{print "Machine reconstruction throughput:", (($1+$2)*$3)/(2*($4+$5)), "filter+predict/s"}' | tee -a average_times.txt

echo $BISMOOTHED_EXECUTED $CORE_COUNT $TIME_AVG_BISMOOTH | awk '{print "Machine reconstruction throughput:", ($1*$2)/$3, "bismoothed/s"}' | tee -a average_times.txt

cd $PATH
