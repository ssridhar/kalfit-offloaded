#!/bin/bash

PROCESSOR_COUNT=`cat /proc/cpuinfo | grep processor | tail -n1 | awk '{ print $3 }'`

mkdir -p output/${HOSTNAME}/scalability
for i in `seq 0 ${PROCESSOR_COUNT}`;
do
  taskset -c 0-${i} ./a.out -f /scratch/dcampora/events/kalman_filter -n 10000 > output/${HOSTNAME}/scalability/${i}.out
done

